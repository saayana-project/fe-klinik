/** @type {import('next').NextConfig} */
const nextConfig = {
    basePath: '',
    assetPrefix: '/',
    rewrites() {
        return [
            {
                source: '/klinik/super-admin/dashboard',
                destination: '/pages/klinik/super-admin/dashboard',
            },
            {
                source: '/klinik/super-admin/pegawai',
                destination: '/pages/klinik/super-admin/pegawai',
            },
            {
                source: '/klinik/administrasi/dashboard',
                destination: '/pages/klinik/administrasi/dashboard',
            },
            {
                source: '/klinik/administrasi/daftar-pasien',
                destination: '/pages/klinik/administrasi/daftar-pasien',
            },
            {
                source: '/klinik/doktor/pasien',
                destination: '/pages/klinik/doktor/pasien',
            },
            {
                source: '/klinik/bidan/daftar-pasien',
                destination: '/pages/klinik/bidan/daftar-pasien',
            },
            {
                source: '/klinik/bidan/daftar-pasien/penanganan',
                destination: '/pages/klinik/bidan/daftar-pasien/penanganan',
            },
            {
                source: '/klinik/bidan/daftar-pasien/rekam-kehamilan',
                destination: '/pages/klinik/bidan/daftar-pasien/kehamilan',
            },
            {
                source: '/klinik/farmasi/dashboard',
                destination: '/pages/klinik/farmasi/dashboard',
            },
            {
                source: '/klinik/farmasi/pasien',
                destination: '/pages/klinik/farmasi/pasien',
            },
        ]
    }
}

module.exports = nextConfig
