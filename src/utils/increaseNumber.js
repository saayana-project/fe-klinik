export function increaseNumber(numStr) {
    let incrementedNumber = (parseInt(numStr, 10) + 1).toString();
    let result = incrementedNumber.padStart(numStr?.length, '0');
    return result;
}