'use client';
import Swal from "sweetalert2";
import apiService from "../../services/api.service";
import Card from "../components/Card";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import Loading from "@/components/Loading";
import Head from "next/head";

export default function Page() {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data != null) {
                if (data.roles == 99) {
                    // router.replace('/super-admin/dashboard')
                    window.location.href = '/klinik/super-admin/dashboard'
                } else if (data.roles == 1) {
                    router.replace('/klinik/administrasi/dashboard')
                } else if (data.roles == 2) {
                    router.replace('/klinik/doktor/dashboard')
                } else if (data.roles == 3) {
                    router.replace('/klinik/bidan/dashboard')
                } else if (data.roles == 5) {
                    router.replace('/klinik/farmasi/dashboard')
                } else {
                    router.push('/')
                }
            }
        }
    }, [router])

    const handleSubmit = (e) => {
        setIsLoading(true);
        e.preventDefault();

        const data = {
            'email': e.target.elements.email.value,
            'password': e.target.elements.password.value
        }

        apiService.postLogin(data)
            .then((res) => {
                setIsLoading(false)
                const data = res.data.data;

                sessionStorage.setItem('user', JSON.stringify(data));
                if (res.data.data?.roles === 99) {
                    window.location.href = '/klinik/super-admin/dashboard';
                } else if (res.data.data?.roles === 1) {
                    window.location.href = '/klinik/administrasi/dashboard';
                } else if (res.data.data?.roles === 2) {
                    window.location.href = '/klinik/doktor/dashboard';
                } else if (res.data.data?.roles === 3) {
                    window.location.href = '/klinik/bidan/dashboard';
                } else if (res.data.data?.roles === 5) {
                    window.location.href = '/klinik/farmasi/dashboard';
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>Hak akses tidak ada.</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    }).then(res => {
                        router.push('/')
                    })
                }
            })
            .catch((err) => {
                setIsLoading(false)
                Swal.fire({
                    icon: "error",
                    title: 'Terjadi Kesalahan!',
                    text: err.response.data?.message,
                    confirmButtonColor: "#a6cbff"
                });
            })
    }

    return (
        <>
            <Head>
                <title>Login - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}
            <div className="page">
                <div className="flex flex-row justify-center items-center" style={{ height: '100%' }}>
                    <Card
                        title={'Login'}
                        subtitle={'Lakukan proses login untuk masuk ke aplikasi.'}
                    >
                        <form onSubmit={handleSubmit}>
                            <label className="form-control w-full mt-5 mb-10">
                                <div className="label">
                                    <span className="label-text font-bold">Email</span>
                                </div>
                                <input type="text" name="email" placeholder="Masukkan email" className="input input-bordered w-full" />
                                <div className="label mt-5">
                                    <span className="label-text font-bold">Password</span>
                                </div>
                                <input type="password" name="password" placeholder="Masukkan password" className="input input-bordered w-full" />
                            </label>
                            <button className="btn btn-primary text-white text-lg w-full" type="submit">Masuk</button>
                        </form>
                    </Card>
                </div>
            </div>
        </>
    )
}