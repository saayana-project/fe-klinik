// pages/_app.js atau pages/_app.tsx
import '../app/globals.scss'; // Sesuaikan path ke file CSS global Anda

function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
}

export default MyApp;