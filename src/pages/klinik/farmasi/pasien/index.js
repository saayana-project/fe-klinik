'use client'
import Breadcrumb from "@/components/Breadcrumb";
import Card from "@/components/Card";
import * as Layout from "@/components/Layout";
import Modal from "@/components/Modal";
import { faArrowRight, faArrowRightLong, faClipboardList, faEye, faFile, faPlus, faRotate, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";
import Swal from "sweetalert2";
import Select from 'react-select'
import Loading from "@/components/Loading";
import apiService from "../../../../../services/api.service";
import Timeline from "@/components/Timeline";

export default function AntrianDoktor() {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false)
    const [openModalDetail, setOpenModalDetail] = useState(false)
    const [detailDataPasien, setDetailDataPasien] = useState(false)
    const [openModalPeriksa, setOpenModalPeriksa] = useState(false)
    const [openTambah, setOpenTambah] = useState(false)
    const [dataAntrian, setDataAntrian] = useState(null)
    const [dataPasien, setDataPasien] = useState([])
    const [isExist, setIsExist] = useState(false)
    const [nik, setNIK] = useState('')
    const [filterText, setFilterText] = useState('');

    const [anamnesa, setAnamnesa] = useState('')
    const [dataDiagnosa, setDataDiagnosa] = useState([])
    const [dataTherapy, setDataTherapy] = useState([])
    const [dataObat, setDataObat] = useState([])

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    const breadcrumbData = [
        { id: 1, value: 'Daftar Pasien', link: '/klinik/farmasi/pasien' },
    ]

    useEffect(() => {
        const data = async () => {
            await getDataPasien()

            await apiService.getDiagnosa(``)
                .then((response) => {
                    const data = response.data.data;

                    // setDataAntrian(data[0]);
                    setDataDiagnosa(data);
                })

            await apiService.getTherapy(``)
                .then((response) => {
                    const data = response.data.data;

                    // setDataAntrian(data[0]);
                    setDataTherapy(data);
                })

            await apiService.getObat(``)
                .then((response) => {
                    const data = response.data.data;

                    // setDataAntrian(data[0]);
                    setDataObat(data);
                })
        }

        data()
    }, [])

    const getDataPasien = () => {
        apiService.getPasienDoktor(`?status=1&date=${moment(new Date()).format('YYYY-MM-DD')}`)
            .then((response) => {
                const data = response.data.data;

                // setDataAntrian(data[0]);
                setDataPasien(data);
                console.log(data)
            })
    }

    const handleModalDetail = (value) => {
        setOpenModalDetail(true)
        setDetailDataPasien(value)
        console.log(value)
    }

    const closeModalDetail = () => {
        setOpenModalDetail(false)
    }

    const handleModalPeriksa = (value) => {
        setOpenModalPeriksa(true)
        setDetailDataPasien(value)
        console.log(value)
    }

    const closeModalPeriksa = () => {
        setOpenModalPeriksa(false)
    }

    return (
        <>
            <Head>
                <title>Daftar Pasien - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-cols-1 mb-5">
                        <Card
                            customClass={null}
                            title={'Daftar Pasien'}
                            subtitle={'Menampilkan daftar pasien.'}
                        >
                            {/* <div>tes</div> */}
                        </Card>
                    </div>

                    <div className="grid grid-cols-1">
                        <Card>
                            <DataTable
                                responsive={true}
                                columns={[
                                    {
                                        name: 'No. Antrian',
                                        selector: row => (row.antrian?.no_antrian)?.slice(11),
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Nama Pasien',
                                        selector: row => row?.nama_pasien,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Tinggi Badan',
                                        selector: row => row?.history[0]?.tinggi_badan ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Berat Badan',
                                        selector: row => row?.history[0]?.berat_badan ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Tensi Darah',
                                        selector: row => row?.history[0]?.tensi_darah ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Anamnesa',
                                        selector: row => row?.history[0]?.anamnesa ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Diagnosa',
                                        selector: row => row?.history[0]?.diagnosa ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Therapy',
                                        selector: row => row?.history[0]?.therapy ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Obat',
                                        selector: row => row?.history[0]?.obat ?? '-',
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Status',
                                        // button: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                        cell: (row) => (
                                            <div>
                                                {
                                                    row?.antrian?.soft_delete == 0 ?
                                                        <div className="rounded-lg bg-error text-white ps-1 pe-1 font-semibold">belum selesai</div>
                                                        : <div className="rounded-lg bg-success text-white ps-1 pe-1 font-semibold">selesai</div>
                                                }
                                            </div>
                                        ),
                                    },
                                    {
                                        name: 'Aksi',
                                        button: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                        cell: (row) => (
                                            <div>
                                                {/* Replace with whatever actions you need to perform */}
                                                {/* <button className="btn btn-primary btn-xs me-1 mb-1" onClick={() => handleModalDetail(row)}><FontAwesomeIcon icon={faEye} /> Detail</button> */}
                                                <button className="btn btn-primary btn-xs" onClick={() => handleModalPeriksa(row)}><FontAwesomeIcon icon={faFile} /> Nota</button>
                                            </div>
                                        ),
                                    },
                                ]}
                                data={dataPasien}
                                pagination
                            />

                            <Modal
                                title={'Nota Pembayaran'}
                                openModal={openModalPeriksa}
                            >
                                <div className="grid grid-cols-1 gap-1 mt-5">
                                    <div>
                                        <div className="overflow-x-auto">
                                            <table className="table">
                                                {/* head */}
                                                <thead>
                                                    <tr>
                                                        <th className="text-black uppercase">Info</th>
                                                        <th className="text-black uppercase">Jumlah</th>
                                                        <th className="text-black uppercase">Harga Satuan</th>
                                                        <th className="text-black uppercase">Total Biaya</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {/* row 1 */}
                                                    <tr>
                                                        <td>Obat 1</td>
                                                        <td>1</td>
                                                        <td>Rp100.000</td>
                                                        <td>Rp100.000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div className="mt-10 flex justify-end">
                                            <div className="text-black uppercase me-20 font-bold">Total Biaya (1 Barang)</div>
                                            <div className="text-black font-bold">Rp100.000</div>
                                        </div>
                                    </div>

                                    <div className="mt-10 text-end">
                                        <button type="button" className="btn btn-error me-2" onClick={closeModalPeriksa}>Tutup</button>
                                        <button type="submit" className="btn btn-info">Selesai</button>
                                    </div>
                                </div>
                            </Modal>
                        </Card>
                    </div>
                </div>
            </Layout.LayoutSuperAdmin>
        </>
    )
}