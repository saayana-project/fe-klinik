'use client'
import * as Layout from "@/components/Layout";
import Card from "@/components/Card"
import Head from "next/head";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react"
import Breadcrumb from "@/components/Breadcrumb";
import apiService from "../../../../../services/api.service";

export default function Dashboard() {
    const router = useRouter();
    const [jmlPasien, setJmlPasien] = useState(null)
    const [jmlPasienDoktor, setJmlPasienDoktor] = useState(null)
    const [jmlPasienBidan, setJmlPasienBidan] = useState(null)

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    useEffect(() => {
        const getData = async () => {
            await apiService.getJmlPasien()
                .then((response) => {
                    const data = response.data;
                    setJmlPasien(data.count_pasien)
                    setJmlPasienDoktor(data.count_pasien_doktor)
                    setJmlPasienBidan(data.count_pasien_bidan)
                    // setJmlPasien(response.data.count)
                })
        }

        getData();
    }, [])

    const breadcrumbData = [
        { id: 1, value: 'Beranda', link: '/klinik/administrasi/dashboard' }
    ]

    return (
        <>
            <Head>
                <title>Dashboard - Klinik Pratama Wiwied Arsari</title>
            </Head>

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-cols-3 gap-4">
                        <Card
                            customClass={null}
                            title={null}
                            subtitle={null}
                        >
                            <div className="flex flex-column justify-between items-center">
                                <div>
                                    <div className="font-bold text-xl">
                                        Jumlah Pasien
                                    </div>
                                    <div className="font-semibold text-md text-gray-400">
                                        Per Hari Ini
                                    </div>
                                </div>
                                <div className="font-bold text-2xl text-primary">
                                    {jmlPasien ?? '0'}
                                </div>
                            </div>
                        </Card>
                        <Card
                            customClass={null}
                            title={null}
                            subtitle={null}
                        >
                            <div className="flex flex-column justify-between items-center">
                                <div>
                                    <div className="font-bold text-xl">
                                        Jumlah Pasien Doktor
                                    </div>
                                    <div className="font-semibold text-md text-gray-400">
                                        Per Hari Ini
                                    </div>
                                </div>
                                <div className="font-bold text-2xl text-success">
                                    {jmlPasienDoktor ?? '0'}
                                </div>
                            </div>
                        </Card>
                        <Card
                            customClass={null}
                            title={null}
                            subtitle={null}
                        >
                            <div className="flex flex-column justify-between items-center">
                                <div>
                                    <div className="font-bold text-xl">
                                        Jumlah Pasien Bidan
                                    </div>
                                    <div className="font-semibold text-md text-gray-400">
                                        Per Hari Ini
                                    </div>
                                </div>
                                <div className="font-bold text-2xl text-error">
                                    {jmlPasienBidan ?? '0'}
                                </div>
                            </div>
                        </Card>
                    </div>

                    <div className="grid grid-cols-4 gap-4">
                        {/* <div style={{ backgroundColor: 'red' }}>01</div> */}
                        {/* <div>09</div> */}
                    </div>
                </div>
            </Layout.LayoutSuperAdmin>

        </>
    )
}