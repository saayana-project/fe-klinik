'use client'
import Breadcrumb from "@/components/Breadcrumb";
import Card from "@/components/Card";
import * as Layout from "@/components/Layout";
import Modal from "@/components/Modal";
import { faArrowRight, faArrowRightLong, faClipboardList, faEye, faListUl, faPlus, faRotate, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import apiService from "../../../../../services/api.service";
import DataTable from "react-data-table-component";
import moment from "moment";
import Swal from "sweetalert2";
import Select from 'react-select'
import Loading from "@/components/Loading";
import { ModalPasienBidanBaru, ModalPasienDoktorBaru } from "@/components/ModalComponents";

export default function DaftarPasien() {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false)
    const [filterStatus, setFilterStatus] = useState(1)
    const [activeTab, setActiveTab] = useState('doktor')
    const [openCekNIK, setOpenCekNIK] = useState(false)
    const [openTambah, setOpenTambah] = useState(false)
    const [dataAntrian, setDataAntrian] = useState(null)
    const [dataPasien, setDataPasien] = useState([])
    const [isExist, setIsExist] = useState(false)
    const [noRegis, setNoRegis] = useState(null)
    const [filterText, setFilterText] = useState('');

    const [nik, setNIK] = useState('')
    const [namaPasien, setNamaPasien] = useState('')
    const [tglLahir, setTglLahir] = useState('')
    const [usiaPasien, setUsiaPasien] = useState('')
    const [tinggiBadan, setTinggiBadan] = useState('')
    const [beratBadan, setBeratBadan] = useState('')
    const [noHP, setNoHP] = useState('')
    const [kerabat, setKerabat] = useState('')
    const [usiaKerabat, setUsiaKerabat] = useState('')
    const [pekerjaan, setPekerjaan] = useState('')
    const [alamat, setAlamat] = useState('')
    const [tensiDarah, setTensiDarah] = useState('')

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    const breadcrumbData = [
        { id: 1, value: 'Pendaftaran Pasien', link: null },
        { id: 2, value: 'Antrian Pasien', link: '/klinik/administrasi/daftar-pasien' }
    ]

    const toggleLoading = (loading) => {
        setIsLoading(loading);
    };

    useEffect(() => {
        const data = async () => {
            await getDataPasien()
        }

        data()
    }, [filterStatus])

    const changeTab = (tabName) => {
        setActiveTab(tabName);

        if (tabName === 'doktor') {
            setFilterStatus(1)
        } else {
            setFilterStatus(2)
        }
    };

    const getDataPasien = () => {
        apiService.getPasien(`?status=${filterStatus}&date=${moment(new Date()).format('YYYY-MM-DD')}`)
            .then((response) => {
                const data = response.data.data;

                setDataPasien(data);
            })
    }

    const searchPasien = dataPasien.filter(item => {
        return (
            item?.no_reg.toLowerCase().includes(filterText.toLowerCase()) ||
            item?.nama.toLowerCase().includes(filterText.toLowerCase())
        );
    });

    const resultData = searchPasien.map((item, index) => ({
        ...item,
    }))

    const handleOpenTambah = () => {
        setOpenTambah(true)
    }

    const handleCloseTambah = () => {
        setOpenTambah(false)
    }

    return (
        <>
            <Head>
                <title>Pendaftaran Pasien - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-cols-1 mb-5">
                        <Card
                            customClass={null}
                            title={'Pendaftaran Pasien'}
                            subtitle={'Melakukan pendaftaran pasien untuk antrian doktor & bidan.'}
                        >
                            {/* <div>tes</div> */}
                        </Card>
                    </div>

                    <div className="grid grid-rows-1">
                        <div className="grid grid-rows-1">
                            <div className="grid grid-cols-1">
                                <div role="tablist" className="tabs tabs-boxed w-96 bg-gray-200 p-0">
                                    <a
                                        role="tab"
                                        onClick={() => changeTab('doktor')}
                                        className={`tab ${activeTab === 'doktor' ? 'bg-success font-semibold text-white' : 'font-semibold'}`}
                                    >
                                        Antrian Doktor
                                    </a>
                                    <a
                                        role="tab"
                                        onClick={() => changeTab('bidan')}
                                        className={`tab ${activeTab === 'bidan' ? 'bg-success font-semibold text-white' : 'font-semibold'}`}
                                    >
                                        Antrian Bidan
                                    </a>
                                </div>
                            </div>
                            <div className="grid grid-cols-1">
                                {
                                    activeTab === 'doktor' ? <>
                                        <Card>
                                            {/* <button className="btn btn-info btn-sm text-white w-28" onClick={handleOpenCek}><FontAwesomeIcon icon={faPlus} />Tambah</button> */}
                                            <div>
                                                <div className="dropdown dropdown-hover">
                                                    <div tabIndex={0} role="button" className="btn btn-info btn-sm text-white w-28 m-1"><FontAwesomeIcon icon={faPlus} />Tambah</div>
                                                    <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
                                                        <li><button className="font-semibold" onClick={handleOpenTambah}><FontAwesomeIcon icon={faPlus} />Pasien Baru</button></li>
                                                        <li><button className="font-semibold"><FontAwesomeIcon icon={faPlus} />Pasien Lama</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="flex justify-between items-center mt-3">
                                                <div className="text-sm italic">Data tampil berdasarkan waktu hari ini.</div>
                                                <input type="text" placeholder="Cari pasien..." className="input input-bordered w-96 bg-white" value={filterText} onChange={(e) => setFilterText(e.target.value)} />
                                            </div>

                                            <ModalPasienDoktorBaru
                                                isOpen={openTambah}
                                                isClose={handleCloseTambah}
                                                onGetDataPasien={() => getDataPasien()}
                                                onLoading={toggleLoading}
                                            />

                                            <DataTable
                                                responsive={true}
                                                columns={[
                                                    {
                                                        name: 'No. Antrian',
                                                        selector: row => <div className="font-bold text-red-600">{(row.no_antrian)?.slice(7)}</div>,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'Nama Pasien',
                                                        selector: row => row.nama,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'No. Registrasi',
                                                        selector: row => <div className="font-bold text-green-700 text-md">{row.no_reg}</div>,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'NIK',
                                                        selector: row => row.nik ?? '-',
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'Alamat',
                                                        selector: row => row.alamat,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'No. HP',
                                                        selector: row => row.no_hp,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                ]}
                                                data={resultData}
                                                pagination
                                            />
                                        </Card>
                                    </> : <div className="grid grid-cols-1">
                                        <Card>
                                            {/* <button className="btn btn-info btn-sm text-white w-28" onClick={handleOpenCek}><FontAwesomeIcon icon={faPlus} />Tambah</button> */}
                                            <div>
                                                <div className="dropdown dropdown-hover">
                                                    <div tabIndex={0} role="button" className="btn btn-info btn-sm text-white w-28 m-1"><FontAwesomeIcon icon={faPlus} />Tambah</div>
                                                    <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
                                                        <li><button className="font-semibold" onClick={handleOpenTambah}><FontAwesomeIcon icon={faPlus} />Pasien Baru</button></li>
                                                        <li><button className="font-semibold"><FontAwesomeIcon icon={faPlus} />Pasien Lama</button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="flex justify-between items-center mt-3">
                                                <div className="text-sm italic">Data tampil berdasarkan waktu hari ini.</div>
                                                <input type="text" placeholder="Cari pasien..." className="input input-bordered w-96 bg-white" value={filterText} onChange={(e) => setFilterText(e.target.value)} />
                                            </div>

                                            <ModalPasienBidanBaru
                                                isOpen={openTambah}
                                                isClose={handleCloseTambah}
                                                onGetDataPasien={() => getDataPasien()}
                                                onLoading={toggleLoading}
                                            />

                                            <DataTable
                                                responsive={true}
                                                columns={[
                                                    {
                                                        name: 'No. Antrian',
                                                        selector: row => <div className="font-bold text-red-600">{(row.no_antrian)?.slice(7)}</div>,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'Nama Pasien',
                                                        selector: row => row.nama,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'No. Registrasi',
                                                        selector: row => <div className="font-bold text-green-700 text-md">{row.no_reg}</div>,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'NIK',
                                                        selector: row => row.nik ?? '-',
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'Alamat',
                                                        selector: row => row.alamat,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                    {
                                                        name: 'No. HP',
                                                        selector: row => row.no_hp,
                                                        sortable: true,
                                                        wrap: true,
                                                        allowOverflow: true,
                                                        grow: 2,
                                                    },
                                                ]}
                                                data={resultData}
                                                pagination
                                            />
                                        </Card>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Layout.LayoutSuperAdmin >
        </>
    )
}