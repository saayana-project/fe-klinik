'use client'
import Breadcrumb from "@/components/Breadcrumb";
import Card from "@/components/Card";
import * as Layout from "@/components/Layout";
import Modal from "@/components/Modal";
import { faArrowRight, faArrowRightLong, faClipboardList, faEye, faPlus, faRotate, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import apiService from "../../../../../../services/api.service";
import DataTable from "react-data-table-component";
import moment from "moment";
import Swal from "sweetalert2";
import Select from 'react-select'
import Loading from "@/components/Loading";

export default function AntrianDoktor() {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false)
    const [openCekNIK, setOpenCekNIK] = useState(false)
    const [openTambah, setOpenTambah] = useState(false)
    const [dataAntrian, setDataAntrian] = useState(null)
    const [dataPasien, setDataPasien] = useState([])
    const [isExist, setIsExist] = useState(false)
    const [nik, setNIK] = useState('')
    const [filterText, setFilterText] = useState('');

    const [namaPasien, setNamaPasien] = useState('')
    const [noRegis, setNoRegis] = useState('')
    const [tglLahir, setTglLahir] = useState('')
    const [usiaPasien, setUsiaPasien] = useState('')
    const [tinggiBadan, setTinggiBadan] = useState('')
    const [beratBadan, setBeratBadan] = useState('')
    const [noHP, setNoHP] = useState('')
    const [kerabat, setKerabat] = useState('')
    const [usiaKerabat, setUsiaKerabat] = useState('')
    const [pekerjaan, setPekerjaan] = useState('')
    const [alamat, setAlamat] = useState('')
    const [tensiDarah, setTensiDarah] = useState('')

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    const breadcrumbData = [
        { id: 1, value: 'Pendaftaran Pasien', link: null },
        { id: 2, value: 'Antrian Doktor', link: '/klinik/administrasi/pendaftaran/antrian-doktor' }
    ]

    useEffect(() => {
        const data = async () => {
            await getDataPasien()
        }

        data()
    }, [])

    useEffect(() => {
        console.log('D' + moment(new Date()).format('YYYYMMDD') + dataAntrian?.no_antrian?.slice(9) + ' ' + dataAntrian?.no_antrian)
    })

    const getDataPasien = () => {
        apiService.getPasien(`?status=1&date=${moment(new Date()).format('YYYY-MM-DD')}`)
            .then((response) => {
                const data = response.data.data;

                setDataAntrian(data[0]);
                setDataPasien(data);
            })
    }

    const searchPasien = dataPasien.filter(item => {
        return (
            item.pasien?.nik.toLowerCase().includes(filterText.toLowerCase()) ||
            item.pasien?.nama_pasien.toLowerCase().includes(filterText.toLowerCase())
        );
    });

    const resultData = searchPasien.map((item, index) => ({
        ...item,
    }))

    const handleOpenCek = () => {
        setOpenCekNIK(true)
    }

    const handleCloseCek = () => {
        setOpenCekNIK(false)
    }

    const handleCekNIK = (e) => {
        e.preventDefault();

        const _nik = e.target.elements.nik.value;

        const data = {
            'nik': _nik
        }

        apiService.postCekNIK(data)
            .then((response) => {
                // setInputNIK(nik)
                const data = response.data?.data[0];

                setNIK(data?.nik)
                setNamaPasien(data?.nama_pasien)
                setNoRegis(data?.no_reg)
                setTglLahir(data?.tgl_lahir)
                setUsiaPasien(data?.u_pasien)
                setTinggiBadan(data?.history?.length > 0 ? data?.history[0]?.tinggi_badan : null)
                setBeratBadan(data?.history?.length > 0 ? data?.history[0]?.berat_badan : null)
                setNoHP(data?.no_hp)
                setKerabat(data?.kerabat)
                setUsiaKerabat(data?.u_kerabat)
                setPekerjaan(data?.pekerjaan)
                setAlamat(data?.alamat)
                setTensiDarah(data?.history?.length > 0 ? data?.history[0]?.tensi_darah : null)

                setOpenCekNIK(false)
                setOpenTambah(true)
                setIsExist(true);
            })
            .catch(response => {
                if (response.data?.data == null) {
                    setNIK(_nik)
                    setNamaPasien('')
                    setNoRegis('')
                    setTglLahir('')
                    setUsiaPasien('')
                    setTinggiBadan('')
                    setBeratBadan('')
                    setNoHP('')
                    setKerabat('')
                    setUsiaKerabat('')
                    setPekerjaan('')
                    setAlamat('')
                    setTensiDarah('')
                }
                setOpenCekNIK(false)
                setOpenTambah(true)
            })
    }

    const handleOpenTambah = () => {
        setOpenTambah(true)
    }

    const handleCloseTambah = () => {
        setOpenTambah(false)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setIsLoading(true)
        setOpenTambah(false)

        const year = moment(new Date).format('YYYY')
        const month = moment(new Date).format('MM')
        const day = moment(new Date).format('DD')

        const data = {
            'no_antrian': 'D' + year + month + day + '00' + e.target.elements.no_antrian.value,
            'no_reg': noRegis,
            'nik': nik,
            'nama_pasien': namaPasien,
            'tgl_lahir': tglLahir,
            'u_pasien': usiaPasien,
            'alamat': alamat,
            'no_hp': noHP,
            'kerabat': kerabat,
            'u_kerabat': usiaKerabat,
            'pekerjaan': pekerjaan,
            'status': '1',
            'berat_badan': beratBadan,
            'tinggi_badan': tinggiBadan,
            'tensi_darah': tensiDarah,
        }

        apiService.postPasienDoktorBaru(data)
            .then((response) => {
                setIsLoading(false)
                getDataPasien();
            })
            .catch((err) => {
                Swal.fire({
                    title: 'Gagal!',
                    html: '<i>' + err.response.data.message + '</i>',
                    icon: 'error',
                    confirmButtonColor: '#0058a8',
                }).then(res => {
                    setIsLoading(false)
                })
            })
    }

    const handleEdit = (e) => {
        e.preventDefault();
        setIsLoading(true)
        setOpenTambah(false)

        const year = moment(new Date).format('YYYY')
        const month = moment(new Date).format('MM')
        const day = moment(new Date).format('DD')

        const data = {
            'no_antrian': 'D' + year + month + day + '00' + e.target.elements.no_antrian.value,
            'status': '1',
            'nik': nik,
            'berat_badan': beratBadan,
            'tinggi_badan': tinggiBadan,
            'tensi_darah': tensiDarah,
        }

        apiService.postPasienExist(data)
            .then((response) => {
                setIsLoading(false)
                getDataPasien();
            })
            .catch((err) => {
                Swal.fire({
                    title: 'Gagal!',
                    html: '<i>' + err.response.data.message + '</i>',
                    icon: 'error',
                    confirmButtonColor: '#0058a8',
                }).then(res => {
                    setIsLoading(false)
                })
            })
    }

    return (
        <>
            <Head>
                <title>Pendaftaran Pasien - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-cols-1 mb-5">
                        <Card
                            customClass={null}
                            title={'Pendaftaran Pasien'}
                            subtitle={'Melakukan pendaftaran pasien untuk antrian doktor.'}
                        >
                            {/* <div>tes</div> */}
                        </Card>
                    </div>

                    <div className="grid grid-cols-1">
                        <Card>
                            <div className="mb-5">
                                <button className="btn btn-info btn-sm text-white" onClick={handleOpenCek}><FontAwesomeIcon icon={faPlus} />Tambah</button>
                                <div className="flex justify-between items-center mt-3">
                                    <div className="text-sm italic">Data tampil berdasarkan waktu hari ini.</div>
                                    <input type="text" placeholder="Cari pasien..." className="input input-bordered w-96 bg-white" value={filterText} onChange={(e) => setFilterText(e.target.value)} />
                                </div>
                                <Modal
                                    title={'Pendaftaran Pasien'}
                                    openModal={openCekNIK}
                                >
                                    <form onSubmit={handleCekNIK}>
                                        <div className="grid grid-cols-1 mt-5">
                                            <div className="label">
                                                <span className="label-text font-bold">NIK :</span>
                                            </div>
                                            <input type="text" name="nik" placeholder="Masukkan nik pasien" className="input input-bordered w-50" autoComplete="off" required />

                                            <div className="mt-10 text-end">
                                                <button type="button" className="btn btn-error me-2" onClick={handleCloseCek}>Tutup</button>
                                                <button type="submit" className="btn btn-success">Selanjutnya <FontAwesomeIcon icon={faArrowRightLong} /></button>
                                            </div>
                                        </div>
                                    </form>
                                </Modal>
                                <Modal
                                    title={'Pendaftaran Pasien'}
                                    openModal={openTambah}
                                >
                                    <form onSubmit={isExist ? handleEdit : handleSubmit}>
                                        <div>
                                            <div className="grid grid-cols-1 gap-4 mt-5">
                                                <div className="italic text-red-500">*data langsung terisi karena NIK telah tersedia</div>
                                            </div>
                                            <div className="grid grid-cols-2 gap-4">
                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">No. Antrian :</span>
                                                    </div>
                                                    <input type="text" name="no_antrian" value={
                                                        'D' + moment(new Date()).format('YYYYMMDD') + dataAntrian?.no_antrian?.slice(9) == dataAntrian?.no_antrian ? (parseInt(dataAntrian?.no_antrian?.slice(11)) + 1) : 1
                                                    } className="input input-bordered w-50" autoComplete="off" readOnly required />
                                                </div>

                                                <div></div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Nama Pasien :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan nama pasien" name="nama_pasien" value={namaPasien} onChange={(e) => setNamaPasien(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">No. Registrasi :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan nomor registrasi" name="no_reg" value={noRegis} onChange={(e) => setNoRegis(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                </div>

                                                <div className="grid grid-cols-2 gap-4">
                                                    <div>
                                                        <div className="label">
                                                            <span className="label-text font-bold">Tanggal Lahir :</span>
                                                        </div>
                                                        <input type="date" placeholder="Masukkan tanggal lahir" name="tgl_lahir" value={tglLahir} onChange={(e) => setTglLahir(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                    </div>
                                                    <div>
                                                        <div className="label">
                                                            <span className="label-text font-bold">Usia Pasien :</span>
                                                        </div>
                                                        <input type="number" placeholder="Masukkan usia" name="u_pasien" value={usiaPasien} onChange={(e) => setUsiaPasien(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">NIK :</span>
                                                    </div>
                                                    <input type="number" placeholder="Masukkan NIK" name="nik" className="input input-bordered w-full" value={nik} autoComplete="off" readOnly={nik != '' ? true : false} required disabled={isExist ? true : false} />
                                                </div>

                                                <div className="grid grid-cols-2 gap-4">
                                                    <div>
                                                        <div className="label">
                                                            <span className="label-text font-bold">Tinggi Badan :</span>
                                                        </div>
                                                        <input type="number" placeholder="Masukkan tinggi badan" name="tinggi_badan" value={tinggiBadan} onChange={(e) => setTinggiBadan(e.target.value)} className="input input-bordered w-full" autoComplete="off" maxLength={3} required />
                                                    </div>
                                                    <div>
                                                        <div className="label">
                                                            <span className="label-text font-bold">Berat Badan :</span>
                                                        </div>
                                                        <input type="number" placeholder="Masukkan berat badan" name="berat_badan" value={beratBadan} onChange={(e) => setBeratBadan(e.target.value)} className="input input-bordered w-full" autoComplete="off" required />
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">No. Telp :</span>
                                                    </div>
                                                    <input type="number" placeholder="Masukkan nomor telepon" name="no_hp" value={noHP} onChange={(e) => setNoHP(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                </div>

                                                <div className="grid grid-cols-2 gap-4">
                                                    <div>
                                                        <div className="label">
                                                            <span className="label-text font-bold">Nama Suami/Istri :</span>
                                                        </div>
                                                        <input type="text" placeholder="Masukkan nama suami/istri" name="kerabat" value={kerabat} onChange={(e) => setKerabat(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                    </div>
                                                    <div>
                                                        <div className="label">
                                                            <span className="label-text font-bold">Usia Suami/Istri :</span>
                                                        </div>
                                                        <input type="number" placeholder="Masukkan usia" name="u_kerabat" value={usiaKerabat} onChange={(e) => setUsiaKerabat(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Pekerjaan :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan pekerjaan" name="pekerjaan" value={pekerjaan} onChange={(e) => setPekerjaan(e.target.value)} className="input input-bordered w-full" autoComplete="off" required disabled={isExist ? true : false} />
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Alamat :</span>
                                                    </div>
                                                    <textarea name="alamat" value={alamat} onChange={(e) => setAlamat(e.target.value)} className="textarea textarea-bordered w-full" rows={3} disabled={isExist ? true : false} ></textarea>
                                                </div>

                                                <div></div>
                                            </div>

                                            <div className="mt-5 mb-3">
                                                <hr />
                                            </div>

                                            <div className="grid grid-cols-2 gap-4">
                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Tensi Darah :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan tensi darah" name="tensi_darah" value={tensiDarah} onChange={(e) => setTensiDarah(e.target.value)} className="input input-bordered w-full" autoComplete="off" required />
                                                </div>

                                                {/* <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Diagnosa :</span>
                                                    </div>
                                                    <Select
                                                        // defaultValue={selectJabatan}
                                                        value={dataDiagnosa.filter((option) => option.value == kodeDiagnosa)}
                                                        placeholder="Pilih Diagnosa"
                                                        options={dataDiagnosa}
                                                        onChange={(e) => handleSelectDiagnosa(e)}
                                                    // components={animatedComponents}
                                                    />
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Anamnesa :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan anamnesa" name="anamnesa" value={anamnesa} onChange={(e) => setAnamnesa(e.target.value)} className="input input-bordered w-full" autoComplete="off"  />
                                                </div>

                                                <div>
                                                    <div className="label">
                                                        <span className="label-text font-bold">Therapy :</span>
                                                    </div>
                                                    <select className="select select-bordered w-full" name="therapy">
                                                        <option selected>Pilih Therapy</option>
                                                        {
                                                            dataTherapy.length > 0 ?
                                                                dataTherapy.map((row, i) => (
                                                                    <option key={i} value={row.kode_therapy}>{row.nama_therapy}</option>
                                                                ))
                                                                : ''
                                                        }
                                                    </select>
                                                </div> */}
                                            </div>

                                        </div>

                                        <div className="mt-10 text-end">
                                            <button type="button" className="btn btn-error me-2" onClick={handleCloseTambah}>Batal</button>
                                            <button type="submit" className="btn btn-info">Simpan</button>
                                        </div>
                                    </form>
                                </Modal>
                            </div>
                            <DataTable
                                responsive={true}
                                columns={[
                                    {
                                        name: 'No. Antrian',
                                        selector: row => (row.no_antrian)?.slice(11),
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Nama Pasien',
                                        selector: row => row?.pasien?.nama_pasien,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'NIK',
                                        selector: row => row?.pasien?.nik,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'No. Registrasi',
                                        selector: row => row?.pasien?.no_reg,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Tanggal Lahir',
                                        selector: row => moment(row?.pasien?.tgl_lahir).format('DD/MM/YYYY'),
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'No. HP',
                                        selector: row => row?.pasien?.no_hp,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                ]}
                                data={resultData}
                                pagination
                            />
                        </Card>
                    </div>
                </div>
            </Layout.LayoutSuperAdmin>
        </>
    )
}