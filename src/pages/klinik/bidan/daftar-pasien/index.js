'use client'
import Breadcrumb from "@/components/Breadcrumb";
import Card from "@/components/Card";
import * as Layout from "@/components/Layout";
import Modal from "@/components/Modal";
import { faArrowRight, faArrowRightLong, faClipboardList, faEye, faListUl, faPlus, faRotate, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import apiService from "../../../../../services/api.service";
import DataTable from "react-data-table-component";
import moment from "moment";
import Swal from "sweetalert2";
import Select from 'react-select'
import Loading from "@/components/Loading";
import { ModalPasienBidanBaru, ModalPasienDoktorBaru, ModalRegistrasiKehamilan } from "@/components/ModalComponents";

export default function DaftarPasien() {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false)
    const [filterTindakan, setFilterTindakan] = useState(null)
    const [activeTab, setActiveTab] = useState('doktor')
    const [openCekNIK, setOpenCekNIK] = useState(false)
    const [openRegistrasi, setOpenRegistrasi] = useState(false)
    const [dataPasien, setDataPasien] = useState([])
    const [dataRegistrasi, setDataRegistrasi] = useState([])
    const [filterText, setFilterText] = useState('');

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    const breadcrumbData = [
        { id: 1, value: 'Daftar Pasien', link: '/klinik/bidan/daftar-pasien' }
    ]

    const toggleLoading = (loading) => {
        setIsLoading(loading);
    };

    useEffect(() => {
        const data = async () => {
            await getDataPasien()
        }

        data()
    }, [filterTindakan])

    const changeTab = (tabName) => {
        setActiveTab(tabName);

        if (tabName === 'doktor') {
            setFilterStatus(1)
        } else {
            setFilterStatus(2)
        }
    };

    const getDataPasien = () => {
        apiService.getPasienBidan(`?date=${moment(new Date()).format('YYYY-MM-DD')}${filterTindakan != null ? '&tindakan=' + filterTindakan : ''}`)
            .then((response) => {
                const data = response.data.data;

                setDataPasien(data);
            })
    }

    const searchPasien = dataPasien.filter(item => {
        return (
            item?.no_reg.toLowerCase().includes(filterText.toLowerCase()) ||
            item?.nama.toLowerCase().includes(filterText.toLowerCase())
        );
    });

    const resultData = searchPasien.map((item, index) => ({
        ...item,
    }))

    const handleOpenRegistrasi = (row) => {
        setOpenRegistrasi(true)
        setDataRegistrasi(row)
    }

    const handleCloseRegistrasi = () => {
        setOpenRegistrasi(false)
    }

    const handlePenanganan = (item) => {
        localStorage.removeItem('data_penanganan');

        const data = JSON.stringify(item.no_reg);
        localStorage.setItem('data_penanganan', data);
        router.push('/klinik/bidan/daftar-pasien/penanganan');
    }

    const handleKehamilan = (item) => {
        localStorage.removeItem('data_kehamilan');

        const data = JSON.stringify(item.no_reg);
        localStorage.setItem('data_kehamilan', data);
        router.push('/klinik/bidan/daftar-pasien/kehamilan');
    }

    return (
        <>
            <Head>
                <title>Pendaftaran Pasien - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-cols-1 mb-5">
                        <Card
                            customClass={null}
                            title={'Daftar Pasien'}
                            subtitle={'Daftar pasien bidan untuk tindakan umum, imunisasi/kms, dan kehamilan.'}
                        >
                            {/* <div>tes</div> */}
                        </Card>
                    </div>

                    <div className="grid grid-rows-1">
                        <div className="grid grid-rows-1">
                            <Card>
                                <div className="grid grid-cols-2">
                                    <div className="flex mb-5 w-full">
                                        <div className="label me-4">
                                            <span className="label-text font-semibold w-full">Tindakan :</span>
                                        </div>
                                        <select className="select select-bordered font-medium lg:w-96 md:w-56" name="tindakan" value={filterTindakan != null ? filterTindakan : ''} onChange={(e) => {
                                            const value = e.target.value === '' ? null : e.target.value;
                                            setFilterTindakan(value);
                                        }}>
                                            <option selected value={''}>Pilih semua</option>
                                            <option value={1}>Umum</option>
                                            <option value={2}>Imunisasi/KMS</option>
                                            <option value={3}>Kehamilan</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="grid grid-cols-1">
                                    <div>
                                        <div className="text-sm italic">Data tampil berdasarkan
                                            {
                                                filterTindakan == 1 ? ' Umum dan ' : filterTindakan == 2 ? ' Imunisasi/KMS dan ' : filterTindakan == 3 ? ' Kehamilan dan ' : ' '
                                            }
                                            waktu hari ini.</div>
                                    </div>
                                    <DataTable
                                        responsive={true}
                                        columns={[
                                            {
                                                name: 'No. Antrian',
                                                selector: row => <div className="font-bold text-red-600">{(row?.antrian?.no_antrian)?.slice(7)}</div>,
                                                sortable: true,
                                                wrap: true,
                                                allowOverflow: true,
                                                grow: 2,
                                            },
                                            {
                                                name: 'Nama Pasien',
                                                selector: row => row.nama,
                                                sortable: true,
                                                wrap: true,
                                                allowOverflow: true,
                                                grow: 2,
                                            },
                                            {
                                                name: 'No. Registrasi',
                                                selector: row => <div className="font-bold text-green-700 text-md">{row.no_reg}</div>,
                                                sortable: true,
                                                wrap: true,
                                                allowOverflow: true,
                                                grow: 2,
                                            },
                                            {
                                                name: 'Penanganan',
                                                selector: row => row.antrian?.tindakan == 1 ?
                                                    <div className="text-info font-semibold">Umum</div> :
                                                    row.antrian?.tindakan == 2 ?
                                                        <div className="text-info font-semibold">Imunisasi/KMS</div> :
                                                        row.antrian?.tindakan == 3 ?
                                                            <div className="text-info font-semibold">Kehamilan</div> : <></>,
                                                sortable: true,
                                                wrap: true,
                                                allowOverflow: true,
                                                grow: 2,
                                            },
                                            {
                                                name: 'Status',
                                                selector: row => row?.antrian?.soft_delete == 0 ? <div className="text-red-500 font-semibold">belum ditangani</div>
                                                    : row?.antrian?.soft_delete == 99 ? <div className="text-green-500 font-semibold">sudah ditangani</div>
                                                        : <div className="text-blue-500 font-semibold">selesai</div>,
                                                sortable: true,
                                                wrap: true,
                                                allowOverflow: true,
                                                grow: 2,
                                            },
                                            {
                                                name: 'Aksi',
                                                selector: row =>
                                                    row?.antrian?.soft_delete != 99 ?
                                                        row.antrian?.tindakan == 1 || row.antrian?.tindakan == 2 ?
                                                            <button className="btn btn-success btn-sm md:btn-xs" onClick={() => handlePenanganan(row)}>Penanganan <FontAwesomeIcon icon={faArrowRight} /></button> : <button className="btn btn-info btn-sm md:btn-xs" onClick={() => handleKehamilan(row)}>Registrasi <FontAwesomeIcon icon={faArrowRight} /></button>
                                                        : <></>,
                                                sortable: true,
                                                wrap: true,
                                                allowOverflow: true,
                                                grow: 2,
                                            },
                                        ]}
                                        data={resultData}
                                        pagination
                                    />

                                    <ModalRegistrasiKehamilan
                                        isOpen={openRegistrasi}
                                        isData={dataRegistrasi}
                                        isClose={handleCloseRegistrasi}
                                        onGetDataPasien={() => getDataPasien()}
                                        onLoading={toggleLoading}
                                    />
                                </div>
                            </Card>
                        </div>
                    </div>
                </div>
            </Layout.LayoutSuperAdmin >
        </>
    )
}