'use client'
import * as Layout from "@/components/Layout";
import Card from "@/components/Card"
import Head from "next/head";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react"
import Breadcrumb from "@/components/Breadcrumb";
import apiService from "../../../../../../services/api.service";
import moment from "moment";
import { faCheck, faChevronLeft, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import DataTable from "react-data-table-component";
import { ModalTambahKMS } from "@/components/ModalComponents";
import Loading from "@/components/Loading";
import Swal from "sweetalert2";

export default function Penanganan() {
    const router = useRouter();
    const [noReg, setNoRegis] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [dataPasien, setDataPasien] = useState([])
    const [activeTab, setActiveTab] = useState('riwayat_kms')
    const [dataKMS, setDataKMS] = useState([])
    const [openTambah, setOpenTambah] = useState(false)
    const [dataModalPasien, setDataModalPasien] = useState([])
    const [idImunisasi, setIdImunisasi] = useState('')
    const [hepatitisB, setHepatitisB] = useState(null)
    const [bcg, setBcg] = useState(null)
    const [polioTetes1, setPolioTetes1] = useState(null)
    const [dptHbHib1, setDptHbHib1] = useState(null)
    const [polioTetes2, setPolioTetes2] = useState(null)
    const [pcv1, setPcv1] = useState(null)
    const [dptHbHib2, setDptHbHib2] = useState(null)
    const [polioTetes3, setPolioTetes3] = useState(null)
    const [pcv2, setPcv2] = useState(null)
    const [dptHbHib3, setDptHbHib3] = useState(null)
    const [polioTetes4, setPolioTetes4] = useState(null)
    const [polioSuntik, setPolioSuntik] = useState(null)
    const [campakRubela, setCampakRubela] = useState(null)
    const [je, setJe] = useState(null)
    const [pcv3, setPcv3] = useState(null)

    const breadcrumbData = [
        { id: 1, value: 'Daftar Pasien', link: '/klinik/bidan/daftar-pasien' },
        { id: 2, value: 'Penanganan', link: '/klinik/bidan/daftar-pasien/penanganan' }
    ]

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    useEffect(() => {
        const data = localStorage.getItem('pasien_kehamilan');
        if (data) {
            setNoRegis(JSON.parse(data));
        }
    }, []);

    useEffect(() => {
        // Make sure to only call the API once noReg is set
        if (noReg) {
            getDataPasien();
        }
    }, [noReg]);

    const getDataPasien = () => {
        apiService.getDetailKehamilan(`?no_reg=${noReg}`)
            .then((response) => {
                const data = response.data.data[0];

                setDataPasien(data);
            })
    }

    const toggleLoading = (loading) => {
        setIsLoading(loading);
    };

    const handleOpenTambah = (item) => {
        setOpenTambah(true)
        setDataModalPasien(item)
    }

    const handleCloseTambah = () => {
        setOpenTambah(false)
    }

    return (
        <>
            <Head>
                <title>Dashboard - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-rows-1 gap-5">
                        <div className="grid grid-cols-1">
                            <Card
                                customClass={null}
                                title={'Penanganan Pasien'}
                                subtitle={'Penanganan pasien tindakan kehamilan.'}
                                isAksi={
                                    <button className="btn btn-success" onClick={() => handlePenangananSelesai(dataPasien?.antrian?.no_antrian)}><FontAwesomeIcon icon={faCheck} /> Penanganan Selesai</button>
                                }
                            >

                            </Card>
                        </div>

                        <div className="grid grid-cols-1">
                            <Card
                                title={'Keterangan Kehamilan'}
                            >
                                {/* <div className="grid grid-rows-1 gap-2 mt-5">
                                    <div className="grid grid-cols-10 items-center">
                                        <div className="font-medium">Nama Anak</div>
                                        <div className="text-center">:</div>
                                        <div>{dataPasien?.nama}</div>
                                    </div>
                                    <div className="grid grid-cols-10 items-center">
                                        <div className="font-medium">Tanggal Lahir</div>
                                        <div className="text-center">:</div>
                                        <div>{moment(dataPasien?.tgl_lahir).format("DD/MM/YYYY")}</div>
                                    </div>
                                    <div className="grid grid-cols-10 items-center">
                                        <div className="font-medium">Umur</div>
                                        <div className="text-center">:</div>
                                        <div>{dataPasien?.umur_pasien}</div>
                                    </div>
                                </div> */}
                            </Card>
                        </div>

                        <div className="grid grid-cols-1">
                            <Card
                                title={"Riwayat Kehamilan"}
                            >
                                <div className="grid grid-rows-1">
                                    <div className="grid grid-cols-1 mt-5">
                                        <div>
                                            <button className="btn btn-info btn-sm text-white w-28" onClick={() => handleOpenTambah(dataPasien)}><FontAwesomeIcon icon={faPlus} />Tambah</button>
                                        </div>
                                        <div>
                                            {/* <DataTable
                                                                responsive={true}
                                                                columns={[
                                                                    {
                                                                        name: 'Umur',
                                                                        selector: row => row.umur_pasien,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'Bulan Penimbangan',
                                                                        selector: row => moment(row.bulan_penimbangan).format('MMMM YYYY'),
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'Berat Badan',
                                                                        selector: row => row.berat_badan,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'N/T',
                                                                        selector: row => row.nt,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'Asi Ekslusif',
                                                                        selector: row => row.asi_ekslusif,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                ]}
                                                                data={dataKMS}
                                                                pagination
                                                            /> */}
                                        </div>
                                    </div>
                                </div>
                            </Card>

                            {/* <ModalTambahKMS 
                                                isOpen={openTambah}
                                                isClose={handleCloseTambah}
                                                onGetDataPasien={() => getDataPasien()}
                                                onGetDataKMS={() => getDataKMS()}
                                                onLoading={toggleLoading}
                                                isData={dataModalPasien}
                                            /> */}
                        </div>
                    </div>
                </div>
            </Layout.LayoutSuperAdmin>

        </>
    )
}