'use client'
import * as Layout from "@/components/Layout";
import Card from "@/components/Card"
import Head from "next/head";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react"
import Breadcrumb from "@/components/Breadcrumb";
import apiService from "../../../../../../services/api.service";
import moment from "moment";
import { faCheck, faChevronLeft, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import DataTable from "react-data-table-component";
import { ModalTambahKMS } from "@/components/ModalComponents";
import Loading from "@/components/Loading";
import Swal from "sweetalert2";

export default function Penanganan() {
    const router = useRouter();
    const [noReg, setNoRegis] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [dataPasien, setDataPasien] = useState([])
    const [activeTab, setActiveTab] = useState('riwayat_kms')
    const [dataKMS, setDataKMS] = useState([])
    const [openTambah, setOpenTambah] = useState(false)
    const [dataModalPasien, setDataModalPasien] = useState([])
    const [idImunisasi, setIdImunisasi] = useState('')
    const [hepatitisB, setHepatitisB] = useState(null)
    const [bcg, setBcg] = useState(null)
    const [polioTetes1, setPolioTetes1] = useState(null)
    const [dptHbHib1, setDptHbHib1] = useState(null)
    const [polioTetes2, setPolioTetes2] = useState(null)
    const [pcv1, setPcv1] = useState(null)
    const [dptHbHib2, setDptHbHib2] = useState(null)
    const [polioTetes3, setPolioTetes3] = useState(null)
    const [pcv2, setPcv2] = useState(null)
    const [dptHbHib3, setDptHbHib3] = useState(null)
    const [polioTetes4, setPolioTetes4] = useState(null)
    const [polioSuntik, setPolioSuntik] = useState(null)
    const [campakRubela, setCampakRubela] = useState(null)
    const [je, setJe] = useState(null)
    const [pcv3, setPcv3] = useState(null)

    const breadcrumbData = [
        { id: 1, value: 'Daftar Pasien', link: '/klinik/bidan/daftar-pasien' },
        { id: 2, value: 'Penanganan', link: '/klinik/bidan/daftar-pasien/penanganan' }
    ]

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    useEffect(() => {
        const data = localStorage.getItem('data_penanganan');
        if (data) {
            setNoRegis(JSON.parse(data));
        }
    }, []);

    useEffect(() => {
        // Make sure to only call the API once noReg is set
        if (noReg) {
            getDataPasien();
            getDataKMS();
            getDataImunisasi();
        }
    }, [noReg]);

    const getDataPasien = () => {
        apiService.getDetailPasienBidan(`?no_reg=${noReg}`)
            .then((response) => {
                const data = response.data.data[0];

                setDataPasien(data);
            })
    }

    const getDataKMS = () => {
        apiService.getKMS(`?no_reg=${noReg}`)
            .then((response) => {
                const data = response.data.data[0]?.rekam_kms;

                setDataKMS(data);
            })
    }

    const getDataImunisasi = () => {
        apiService.getImunisasi(`?no_reg=${noReg}`)
            .then((response) => {
                const data = response.data.data;

                setIdImunisasi(data?.no_reg)
                setHepatitisB(data?.hepatitis_b);
                setBcg(data?.bcg);
                setPolioTetes1(data?.polio_tetes_1);
                setDptHbHib1(data?.dpt_hb_hib_1);
                setPolioTetes2(data?.polio_tetes_2);
                setPcv1(data?.pcv_1);
                setDptHbHib2(data?.dpt_hb_hib_2);
                setPolioTetes3(data?.polio_tetes_3);
                setPcv2(data?.pcv_2);
                setDptHbHib3(data?.dpt_hb_hib_3);
                setPolioTetes4(data?.polio_tetes_4);
                setPolioSuntik(data?.polio_suntik_ipv);
                setCampakRubela(data?.campak_rubela);
                setJe(data?.je);
                setPcv3(data?.pcv_3);
            })
    }

    const toggleLoading = (loading) => {
        setIsLoading(loading);
    };


    const changeTab = (tabName) => {
        setActiveTab(tabName);

        // if (tabName === 'doktor') {
        //     setFilterStatus(1)
        // } else {
        //     setFilterStatus(2)
        // }
    };

    const handleOpenTambah = (item) => {
        setOpenTambah(true)
        setDataModalPasien(item)
    }

    const handleCloseTambah = () => {
        setOpenTambah(false)
    }

    const handlePenangananSelesai = (id) => {
        Swal.fire({
            title: "Yakin?",
            text: "Apakah Anda yakin penanganan telah selesai?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#0058a8",
            cancelButtonColor: "#d33",
            confirmButtonText: "Selesai",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                apiService.putPenangananBidan(id)
                    .then((response) => {
                        router.push('/klinik/bidan/daftar-pasien')
                    })
                    .catch((err) => {
                        if (err.response && err.response.data && typeof err.response.data === 'object') {
                            const errors = err.response.data;
                            let errorMessage = Object.keys(errors).map(key => {
                                if (Array.isArray(errors[key])) {
                                    return `${key}: ${errors[key].join(', ')}`;
                                } else {
                                    return `${key}: ${errors[key].toString()}`;
                                }
                            }).join('<br/>');

                            Swal.fire({
                                title: 'Gagal!',
                                html: `<i>${errorMessage}</i>`,
                                icon: 'error',
                                confirmButtonColor: '#0058a8',
                            });
                        } else if (err.message) {
                            Swal.fire({
                                title: 'Gagal!',
                                html: `<i>${err.message}</i>`,
                                icon: 'error',
                                confirmButtonColor: '#0058a8',
                            });
                        } else {
                            Swal.fire({
                                title: 'Gagal!',
                                html: '<i>An unexpected error occurred</i>',
                                icon: 'error',
                                confirmButtonColor: '#0058a8',
                            });
                        }
                    })
            }
        });
    }

    const handleHepatitisB = () => {
        if (hepatitisB != null) {
            return false;
        }

        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'hepatitis_b': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleBcg = () => {
        if (bcg != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'bcg': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePolio1 = () => {
        if (polioTetes1 != null) {
            return false;
        }

        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'polio_tetes_1': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleDptHbHib1 = () => {
        if (dptHbHib1 != null) {
            return false;
        }

        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'dpt_hb_hib_1': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePolio2 = () => {
        if (polioTetes2 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'polio_tetes_2': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePcv1 = () => {
        if (pcv1 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'pcv_1': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleDptHbHib2 = () => {
        if (dptHbHib2 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'dpt_hb_hib_2': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePolio3 = () => {
        if (polioTetes3 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'polio_tetes_3': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePcv2 = () => {
        if (pcv2 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'pcv_2': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleDptHbHib3 = () => {
        if (dptHbHib3 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'dpt_hb_hib_3': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePolio4 = () => {
        if (polioTetes4 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'polio_tetes_4': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleSuntik = () => {
        if (polioSuntik != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'polio_suntik_ipv': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleCampakRubela = () => {
        if (campakRubela != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'campak_rubela': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handleJe = () => {
        if (je != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'je': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    const handlePcv3 = () => {
        if (pcv3 != null) {
            return false;
        }
        setIsLoading(true)

        const today = new Date;
        const formatDate = moment(today).format('YYYY-MM-DD');

        const data = {
            'no_reg': idImunisasi,
            'pcv_3': formatDate,
        }

        apiService.postUpdateImunisasi(data)
            .then((response) => {
                setIsLoading(false);
                getDataImunisasi();
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                setIsLoading(false)
            })
    }

    return (
        <>
            <Head>
                <title>Dashboard - Klinik Pratama Wiwied Arsari</title>
            </Head>

            {isLoading ? <Loading /> : ''}

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-rows-1 gap-5">
                        <div className="grid grid-cols-1">
                            <Card
                                customClass={null}
                                title={'Penanganan Pasien'}
                                subtitle={'Penanganan pasien tindakan imunisasi/kms.'}
                                isAksi={
                                    <button className="btn btn-success" onClick={() => handlePenangananSelesai(dataPasien?.antrian?.no_antrian)}><FontAwesomeIcon icon={faCheck} /> Penanganan Selesai</button>
                                }
                            >

                            </Card>
                        </div>

                        <div className="grid grid-cols-1">
                            <Card
                                title={'Imunisasi'}
                            >
                                <div className="grid grid-rows-1 gap-2 mt-5">
                                    <div className="grid grid-cols-10 items-center">
                                        <div className="font-medium">Nama Anak</div>
                                        <div className="text-center">:</div>
                                        <div>{dataPasien?.nama}</div>
                                    </div>
                                    <div className="grid grid-cols-10 items-center">
                                        <div className="font-medium">Tanggal Lahir</div>
                                        <div className="text-center">:</div>
                                        <div>{moment(dataPasien?.tgl_lahir).format("DD/MM/YYYY")}</div>
                                    </div>
                                    <div className="grid grid-cols-10 items-center">
                                        <div className="font-medium">Umur</div>
                                        <div className="text-center">:</div>
                                        <div>{dataPasien?.umur_pasien}</div>
                                    </div>
                                </div>
                            </Card>
                        </div>

                        <div className="grid grid-cols-1">
                            <div role="tablist" className="tabs tabs-boxed w-1/2 bg-gray-200 p-0">
                                <a
                                    role="tab"
                                    onClick={() => changeTab('riwayat_kms')}
                                    className={`tab ${activeTab === 'riwayat_kms' ? 'bg-success font-semibold text-white' : 'font-semibold'}`}
                                >
                                    Riwayat KMS
                                </a>
                                <a
                                    role="tab"
                                    onClick={() => changeTab('status_vaksin')}
                                    className={`tab ${activeTab === 'status_vaksin' ? 'bg-success font-semibold text-white' : 'font-semibold'}`}
                                >
                                    Status Vaksin
                                </a>
                                <a
                                    role="tab"
                                    onClick={() => changeTab('jadwal_vaksin')}
                                    className={`tab ${activeTab === 'jadwal_vaksin' ? 'bg-success font-semibold text-white' : 'font-semibold'}`}
                                >
                                    Jadwal Vaksin
                                </a>
                            </div>
                            {
                                activeTab === 'riwayat_kms' ?
                                    <div className="grid grid-rows-1">
                                        <div className="grid grid-cols-1">
                                            <Card
                                                title={"Riwayat KMS"}
                                            >
                                                <div className="grid grid-rows-1">
                                                    <div className="grid grid-cols-1 mt-5">
                                                        <div>
                                                            <button className="btn btn-info btn-sm text-white w-28" onClick={() => handleOpenTambah(dataPasien)}><FontAwesomeIcon icon={faPlus} />Tambah</button>
                                                        </div>
                                                        <div>
                                                            <DataTable
                                                                responsive={true}
                                                                columns={[
                                                                    {
                                                                        name: 'Umur',
                                                                        selector: row => row.umur_pasien,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'Bulan Penimbangan',
                                                                        selector: row => moment(row.bulan_penimbangan).format('MMMM YYYY'),
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'Berat Badan',
                                                                        selector: row => row.berat_badan,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'N/T',
                                                                        selector: row => row.nt,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                    {
                                                                        name: 'Asi Ekslusif',
                                                                        selector: row => row.asi_ekslusif,
                                                                        sortable: false,
                                                                        wrap: true,
                                                                        allowOverflow: true,
                                                                        grow: 2,
                                                                    },
                                                                ]}
                                                                data={dataKMS}
                                                                pagination
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Card>

                                            <ModalTambahKMS
                                                isOpen={openTambah}
                                                isClose={handleCloseTambah}
                                                onGetDataPasien={() => getDataPasien()}
                                                onGetDataKMS={() => getDataKMS()}
                                                onLoading={toggleLoading}
                                                isData={dataModalPasien}
                                            />
                                        </div>
                                    </div>
                                    : activeTab === 'status_vaksin' ?
                                        <div className="grid grid-rows-1">
                                            <div className="grid grid-cols-1">
                                                <Card
                                                    title={"Status Vaksin"}
                                                    subtitle={"Ketuk tombol untuk mengubah status vaksin."}
                                                >
                                                    <div className="grid grid-rows-1">
                                                        <div className="grid grid-cols-4 gap-5 mt-5">
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Hepatitis B {'(< 24 jam)'}</div>
                                                                <button className={`btn ${hepatitisB != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleHepatitisB()}>
                                                                    {hepatitisB != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    hepatitisB != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(hepatitisB).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">BCG</div>
                                                                <button className={`btn ${bcg != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleBcg()}>
                                                                    {bcg != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    bcg != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(bcg).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Polio 1</div>
                                                                <button className={`btn ${polioTetes1 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePolio1()}>
                                                                    {polioTetes1 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    polioTetes1 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(polioTetes1).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">DPT-HB-Hib 1</div>
                                                                <button className={`btn ${dptHbHib1 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleDptHbHib1()}>
                                                                    {dptHbHib1 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    dptHbHib1 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(dptHbHib1).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="grid grid-cols-4 gap-5 mt-5">
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Polio Tetes 2</div>
                                                                <button className={`btn ${polioTetes2 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePolio2()}>
                                                                    {polioTetes2 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    polioTetes2 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(polioTetes2).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">PCV 1</div>
                                                                <button className={`btn ${pcv1 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePcv1()}>
                                                                    {pcv1 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    pcv1 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(pcv1).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">DPT-HB-Hib 2</div>
                                                                <button className={`btn ${dptHbHib2 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleDptHbHib2()}>
                                                                    {dptHbHib2 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    dptHbHib2 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(dptHbHib2).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Polio Tetes 3</div>
                                                                <button className={`btn ${polioTetes3 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePolio3()}>
                                                                    {polioTetes3 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    polioTetes3 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(polioTetes3).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="grid grid-cols-4 gap-5 mt-5">
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">PCV 2</div>
                                                                <button className={`btn ${pcv2 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePcv2()}>
                                                                    {pcv2 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    pcv2 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(pcv2).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">DPT-HB-Hib 3</div>
                                                                <button className={`btn ${dptHbHib3 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleDptHbHib3()}>
                                                                    {dptHbHib3 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    dptHbHib3 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(dptHbHib3).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Polio Tetes 4</div>
                                                                <button className={`btn ${polioTetes4 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePolio4()}>
                                                                    {polioTetes4 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    polioTetes4 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(polioTetes4).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Polio Suntik (IPV)</div>
                                                                <button className={`btn ${polioSuntik != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleSuntik()}>
                                                                    {polioSuntik != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    polioSuntik != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(polioSuntik).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="grid grid-cols-4 gap-5 mt-5">
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">Campak-Rubella</div>
                                                                <button className={`btn ${campakRubela != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleCampakRubela()}>
                                                                    {campakRubela != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    campakRubela != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(campakRubela).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">JE</div>
                                                                <button className={`btn ${je != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handleJe()}>
                                                                    {je != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    je != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(je).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                            <div className="flex flex-col items-center">
                                                                <div className="font-medium mb-2">PCV 3</div>
                                                                <button className={`btn ${pcv3 != null ? 'btn-success' : 'btn-error'} text-white lg:w-56 md:w-36`} onClick={() => handlePcv3()}>
                                                                    {pcv3 != null ? 'Sudah' : 'Belum'}
                                                                </button>
                                                                {
                                                                    pcv3 != null ?
                                                                        <div className="font-medium mt-2">pada tanggal {moment(pcv3).format('DD/MM/YYYY')}</div>
                                                                        : <></>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>
                                        </div>
                                        : <div className="grid grid-rows-1">
                                            <div className="grid grid-cols-1">
                                                <Card
                                                    title={"Jadwal Vaksin"}
                                                >
                                                    <div className="grid grid-rows-1">
                                                        <div className="grid grid-cols-1">
                                                            <table className="min-w-full table-auto border-collapse border border-black mt-5">
                                                                <thead className="bg-green-100">
                                                                    <tr>
                                                                        <th className="border border-black font-medium px-4 py-2 text-left">UMUR (BULAN)</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">0</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">1</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">2</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">3</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">4</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">5</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">6</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">7</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">8</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">9</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">10</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">11</th>
                                                                        <th className="border border-black font-medium px-4 py-2 w-24">12+***</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">Vaksin</td>
                                                                        <td className="border border-black font-bold px-4 py-2 text-center bg-blue-100" colSpan={13}>Tanggal Pemberian Imunisasi</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">Hepatitis B {'(< 24 jam)'}</td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">BCG</td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*Polio tetes 1</td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*DPT-HB-Hib 1</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*Polio tetes 2</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">** PCV 1</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*DPT-HB-Hib 2</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 "></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*Polio tetes 3</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 "></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">** PCV 2</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 "></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*DPT-HB-Hib 3</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">*Polio tes 4</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">Polio Suntuk (IPV)</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">Campak-Rubella</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">**JE</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                        <td className="border border-black px-4 py-2 bg-yellow-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-red-500"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="border border-black font-medium px-4 py-2 bg-blue-100">** PCV 3</td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300" ></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2 bg-gray-300"></td>
                                                                        <td className="border border-black px-4 py-2"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                            <div className="mt-10">
                                                                <div className="flex items-center w-full">
                                                                    <div className="border border-black me-4 w-24 h-7"></div>
                                                                    <div>Jadwal tepat pemberian imunisasi rutin lengkap</div>
                                                                </div>
                                                                <div className="flex items-center w-full">
                                                                    <div className="border border-black me-4 w-24 h-7 bg-yellow-300"></div>
                                                                    <div>Waktu yang masih diperbolehkan untuk pemberian imunisasi rutin lengkap</div>
                                                                </div>
                                                                <div className="flex items-center w-full">
                                                                    <div className="border border-black me-4 w-24 h-7 bg-red-500"></div>
                                                                    <div>Waktu pemberian imunisasi bagi anak di atas kelas 1 tahun yang belum lengkap</div>
                                                                </div>
                                                                <div className="flex items-center w-full">
                                                                    <div className="border border-black me-4 w-24 h-7 bg-gray-300"></div>
                                                                    <div>Waktu yang tidak diperbolehkan untuk pemberian imunisasi rutin lengkap</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            </div>
                                        </div>
                            }
                        </div>
                    </div>
                </div>
            </Layout.LayoutSuperAdmin>

        </>
    )
}