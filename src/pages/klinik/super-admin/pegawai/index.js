'use client'
import * as Layout from "@/components/Layout";
import Card from "@/components/Card"
import Head from "next/head";
import { useRouter } from "next/navigation";
import { Children, useEffect, useState } from "react"
import Breadcrumb from "@/components/Breadcrumb";
import Loading from "@/components/Loading";
import DataTable from "react-data-table-component";
import apiService from "../../../../../services/api.service";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import Modal from "@/components/Modal";
import Swal from "sweetalert2";

export default function Dashboard() {
    const router = useRouter();
    const [dataJabatan, setDataJabatan] = useState([]);
    const [dataPegawai, setDataPegawai] = useState([]);
    const [modalTambah, setModalTambah] = useState(false);

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            }
        }
    }, [router])

    useEffect(() => {
        const getData = async () => {
            await apiService.getJabatan(``)
                .then((response) => {
                    const data = response.data.data;

                    setDataJabatan(data)
                })

            await getDataPegawai();
        }

        getData()
    }, [])

    const getDataPegawai = () => {
        apiService.getPegawai(``)
            .then((response) => {
                const data = response.data.data;

                const resultData = data.map((item, i) => ({
                    ...item,
                    rowNumber: i + 1
                }))

                setDataPegawai(resultData)
            })
    }

    const handleOpenTambah = () => {
        setModalTambah(true)
    }

    const handleCloseTambah = () => {
        setModalTambah(false)
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            'nama_pegawai': e.target.elements.nama_pegawai.value,
            'tgl_lahir': e.target.elements.tgl_lahir.value,
            'alamat': e.target.elements.alamat.value,
            'email': e.target.elements.email.value,
            'password': e.target.elements.password.value,
            'kode_jabatan': e.target.elements.kode_jabatan.value,
        }

        apiService.postPegawai(data)
            .then(res => {
                Swal.fire({
                    icon: "success",
                    title: "Berhasil!",
                    text: res.data.message,
                    confirmButtonColor: "#a6cbff"
                }).then(() => {
                    setModalTambah(false)
                    getDataPegawai();
                });
            })
            .catch(err => {
                Swal.fire({
                    title: 'Gagal!',
                    html: '<i>' + err.response.data.message + '</i>',
                    icon: 'error',
                    confirmButtonColor: '#0058a8',
                }).then(res => {
                    setModalTambah(false)
                })
            })
    }

    const handleDelete = (value) => {
        const data = {
            'email': value
        }
        Swal.fire({
            title: "Apakah Anda yakin?",
            text: "Anda tidak akan dapat mengembalikan ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4267AC",
            cancelButtonColor: "#FF595E",
            confirmButtonText: "Hapus",
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.isConfirmed) {
                apiService.deletePegawai(data)
                    .then((response) => {
                        console.log(response)
                        Swal.fire({
                            icon: "success",
                            title: "Sukses!",
                            text: 'Berhasil menghapus data',
                            confirmButtonColor: "#4267AC"
                        }).then(() => {
                            getDataPegawai();
                        });
                    })
            }
        });
    }

    const breadcrumbData = [
        { id: 1, value: 'Daftar Pegawai', link: '/klinik/super-admin/pegawai' }
    ]

    return (
        <>
            <Head>
                <title>Daftar Pegawai - Klinik Pratama Wiwied Arsari</title>
            </Head>

            <Layout.LayoutSuperAdmin>
                <div className="p-4">
                    <Breadcrumb
                        breadcrumbData={breadcrumbData}
                    />

                    <div className="grid grid-cols-1 mb-5">
                        <Card
                            customClass={null}
                            title={'Daftar Pegawai'}
                            subtitle={'Menampilkan daftar pegawai serta dapat memasukkan data, mengubah data, dan menghapus data pegawai.'}
                        >
                            {/* <div>tes</div> */}
                        </Card>
                    </div>

                    <div className="grid grid-cols-1">
                        <Card>
                            <div className="mb-5">
                                <button className="btn btn-info btn-sm" onClick={handleOpenTambah}><FontAwesomeIcon icon={faPlus} />Tambah</button>
                                <Modal
                                    // modalID={'my_modal_4'}
                                    title={'Tambah Pegawai'}
                                    openModal={modalTambah}
                                >
                                    <div className="grid grid-cols-1 mt-5">
                                        <form onSubmit={handleSubmit}>
                                            <div>
                                                <div className="mb-5">
                                                    <div className="label">
                                                        <span className="label-text font-bold">Nama :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan nama pegawai" name="nama_pegawai" className="input input-bordered w-full" autoComplete="off" required />
                                                </div>

                                                <div className="mb-5">
                                                    <div className="label">
                                                        <span className="label-text font-bold">Tanggal Lahir :</span>
                                                    </div>
                                                    <input type="date" name="tgl_lahir" className="input input-bordered w-full" autoComplete="off" required />
                                                </div>

                                                <div className="mb-5">
                                                    <div className="label">
                                                        <span className="label-text font-bold">Alamat :</span>
                                                    </div>
                                                    <input type="text" placeholder="Masukkan alamat" name="alamat" className="input input-bordered w-full" autoComplete="off" required />
                                                </div>

                                                <div className="mb-5">
                                                    <div className="label">
                                                        <span className="label-text font-bold">Email :</span>
                                                    </div>
                                                    <input type="email" placeholder="Masukkan email" name="email" className="input input-bordered w-full" autoComplete="off" required />
                                                </div>

                                                <div className="mb-5">
                                                    <div className="label">
                                                        <span className="label-text font-bold">Password :</span>
                                                    </div>
                                                    <input type="password" placeholder="Masukkan password" name="password" className="input input-bordered w-full" autoComplete="off" required />
                                                </div>

                                                <div className="mb-5">
                                                    <div className="label">
                                                        <span className="label-text font-bold">Jabatan :</span>
                                                    </div>
                                                    <select className="select select-bordered w-full" name="kode_jabatan">
                                                        <option disabled selected>Pilih Jabatan</option>
                                                        {
                                                            dataJabatan.length > 0 ?
                                                                dataJabatan.map((row, i) => (
                                                                    <option key={i} value={row.kode_jabatan}>{row.nama_jabatan}</option>
                                                                ))
                                                                : ''
                                                        }
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="mt-10 text-end">
                                                <button type="button" className="btn btn-error me-2" onClick={handleCloseTambah}>Batal</button>
                                                <button type="submit" className="btn btn-info">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </Modal>
                            </div>
                            <DataTable
                                responsive={true}
                                columns={[
                                    {
                                        name: 'No.',
                                        selector: row => row.rowNumber,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Nama',
                                        selector: row => row.nama_pegawai,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Tanggal Lahir',
                                        selector: row => moment(row.tgl_lahir).format('DD MMMM YYYY'),
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Alamat',
                                        selector: row => row.alamat,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Email',
                                        selector: row => row.email,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Roles',
                                        selector: row => row.jabatan?.nama_jabatan,
                                        sortable: true,
                                        wrap: true,
                                        allowOverflow: true,
                                        grow: 2,
                                    },
                                    {
                                        name: 'Aksi',
                                        button: true,
                                        width: '200px',
                                        cell: (row) => (
                                            <div>
                                                {/* Replace with whatever actions you need to perform */}
                                                {/* <button className="btn btn-success m-2"><FontAwesomeIcon icon={faEdit} /> Ubah</button> */}
                                                <button className="btn btn-error btn-sm" onClick={() => handleDelete(row.email)}><FontAwesomeIcon icon={faTrashAlt} /></button>
                                            </div>
                                        ),
                                    },
                                ]}
                                data={dataPegawai}
                                pagination
                            />
                        </Card>
                    </div>
                </div >
            </Layout.LayoutSuperAdmin >

        </>
    )
}