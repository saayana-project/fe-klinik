import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Card({ children, title, subtitle, isLogin, customClass, isAksi }) {
    return (
        <>
            <div className={`card bg-white ${isLogin ? 'shadow-md' : 'shadow-sm'} ${customClass ? customClass : ''}`}>
                <div className="card-body">
                    {
                        isAksi != null ? <>
                            <div className="flex justify-between items-center">
                                <div>
                                    {title ? <h2 className="card-title text-2xl">{title}</h2> : ''}
                                    {subtitle ? <p className="text-gray-400">{subtitle}</p> : ''}
                                </div>
                                <div>
                                    {isAksi}
                                </div>
                            </div>
                        </>
                            :
                            <>
                                {title ? <h2 className="card-title text-2xl">{title}</h2> : ''}
                                {subtitle ? <p className="text-gray-400">{subtitle}</p> : ''}
                            </>
                    }
                    {children}
                </div>
            </div>
        </>
    )
}