import moment from "moment";

export default function Timeline({ data }) {
    return (
        <ul className="timeline timeline-snap-icon max-md:timeline-compact timeline-vertical">
            {data?.map((item, index) => (
                <li key={index}>
                    <div className="timeline-middle">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="h-5 w-5"><path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clipRule="evenodd" /></svg>
                    </div>
                    {/* Gunakan conditional rendering untuk menentukan class */}
                    <div className={index % 2 === 0 ? "timeline-start md:text-end mb-10" : "timeline-end mb-10"}>
                        <time className="font-mono italic">{moment(item.created_at).format('DD/MM/YYYY')}</time>
                        <div>
                            <div className="text-lg font-black">Anamnesa</div>
                            <div>{item?.anamnesa ?? '-'}</div>
                        </div>
                        <div>
                            <div className="text-lg font-black">Diagnosa</div>
                            <div>{item?.diagnosa ?? '-'}</div>
                        </div>
                        <div>
                            <div className="text-lg font-black">Therapy</div>
                            <div>{item?.therapy ?? '-'}</div>
                        </div>
                        <div>
                            <div className="text-lg font-black">Obat</div>
                            <div>{item?.obat ?? '-'}</div>
                        </div>
                    </div>
                    <hr />
                </li>
            ))}
        </ul>
    )
}