export default function Loading() {
    return (
        <>
            <div className="loading-overlay flex justify-center">
                <span className="loading loading-spinner loading-lg text-warning"></span>
            </div>
        </>
    )
}