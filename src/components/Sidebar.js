'use client'
import Image from "next/image";
import { useEffect, useState } from "react";
import Link from 'next/link'
// import logoKlinik from '../../public/assets/img/logo.png';
// import Logo from ""

export default function Sidebar() {
    const [rolesActive, setRolesActive] = useState(null);
    const [menu, setMenu] = useState([]);

    useEffect(() => {
        const data = sessionStorage.getItem('user')
        const roles = JSON.parse(data)?.roles;

        if (roles === 99) {
            const data = [
                { id: 1, link: '/klinik/super-admin/dashboard', menu_name: 'Beranda', isMultiple: false },
                { id: 2, link: '/klinik/super-admin/pegawai', menu_name: 'Daftar Pegawai', isMultiple: false },
            ]

            setMenu(data)
        } else if (roles === 1) {
            const data = [
                { id: 1, link: '/klinik/administrasi/dashboard', menu_name: 'Beranda', isMultiple: false },
                { id: 1, link: '/klinik/administrasi/daftar-pasien', menu_name: 'Daftar Pasien', isMultiple: false },
                // {
                //     id: 2, link: null, menu_name: 'Pendaftaran', isMultiple: true, submenu: [
                //         {
                //             id: 1, submenu_name: 'Antrian Doktor', link: '/klinik/administrasi/pendaftaran/antrian-doktor'
                //         },
                //         {
                //             id: 2, submenu_name: 'Antrian Bidan', link: '/klinik/administrasi/pendaftaran/antrian-bidan'
                //         }
                //     ]
                // },
            ]

            setMenu(data)
        } else if (roles === 2) {
            const data = [
                { id: 1, link: '/klinik/doktor/dashboard', menu_name: 'Beranda', isMultiple: false },
                { id: 2, link: '/klinik/doktor/pasien', menu_name: 'Pasien', isMultiple: false },
            ]

            setMenu(data)
        } else if (roles === 3) {
            const data = [
                { id: 1, link: '/klinik/bidan/dashboard', menu_name: 'Beranda', isMultiple: false },
                { id: 2, link: '/klinik/bidan/daftar-pasien', menu_name: 'Pasien', isMultiple: false },
            ]

            setMenu(data)
        } else if (roles === 5) {
            const data = [
                { id: 1, link: '/klinik/farmasi/dashboard', menu_name: 'Beranda', isMultiple: false },
                { id: 2, link: '/klinik/farmasi/pasien', menu_name: 'Pasien', isMultiple: false },
            ]

            setMenu(data)
        } else {

        }
    }, [])

    return (
        <>
            <div className="drawer-side">
                <label htmlFor="my-drawer-2" aria-label="close sidebar" className="drawer-overlay"></label>
                <ul className="menu p-4 w-64 min-h-full bg-white text-base-content font-semibold">
                    <a href="">
                        <div className="flex flex-row items-center mb-5">
                            <img src={'/assets/img/logo-klinik.png'} className="w-16 h-auto me-2" />
                            <div className="font-bold text-md">Klinik Pratama Wiwied Arsari</div>
                        </div>
                    </a>
                    {
                        menu.length > 0 ?
                            menu.map((row, i) => (
                                <li key={row.id}>
                                    {
                                        row.isMultiple ?
                                            <details close>
                                                <summary>Pendaftaran</summary>
                                                <ul>
                                                    {row.submenu?.map((submenu, index) => (
                                                        <>
                                                            <li><Link href={submenu.link}>{submenu.submenu_name}</Link></li>
                                                        </>
                                                    ))}
                                                </ul>
                                            </details>
                                            : <Link className="active:bg-error" href={row.link}>{row.menu_name}</Link>
                                    }
                                </li>
                            ))
                            : <></>
                    }
                </ul>
            </div>
        </>
    )
}