export default function Modal({ children, openModal, title, subtitle }) {
    return (
        <>
            <dialog className="modal" open={openModal} style={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}>
                <div className="modal-box w-11/12 max-w-5xl">
                    <div className="mb-10">
                        <h2 className="card-title text-2xl">{title}</h2>
                        {subtitle != null ? <div className="text-md text-gray-400">{subtitle}</div> : <></>}
                    </div>

                    {children}
                </div>
            </dialog >
        </>
    )
}