import { faBars, faChevronDown, faHome } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useRouter } from "next/router"
import { useEffect, useState } from "react";
import Loading from "./Loading";
import Sidebar from "./Sidebar";

export const LayoutSuperAdmin = ({ children }) => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false)
    const [userLogin, setUserLogin] = useState(null)
    const [rolesActive, setRolesActive] = useState(null)

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const data = JSON.parse(sessionStorage.getItem('user'))
            setUserLogin(data)

            const roles = data?.roles;

            if (data == null || data == 'undefined' || data == '') {
                router.replace('/')
            } else {
                if (roles === 99) {
                    setRolesActive('super-admin')
                } else if (roles === 1) {
                    setRolesActive('admin')
                } else {

                }
            }
        }
    }, [router])

    const handleLogout = () => {
        setIsLoading(true)
        setTimeout(() => {
            setIsLoading(false)
            sessionStorage.removeItem('user')
            router.replace('/')
        }, 2000)
    }

    return (
        <>
            {isLoading ? <Loading /> : ''}
            <div className="drawer lg:drawer-open">
                <input id="my-drawer-2" type="checkbox" className="drawer-toggle" />
                <div className="drawer-content">
                    <div className="navbar bg-white">
                        <label htmlFor="my-drawer-2" className="btn btn-secondary drawer-button lg:hidden m-5"><FontAwesomeIcon icon={faBars} /> Menu</label>
                        <div className="flex-1">
                            {/* <a className="btn btn-ghost text-xl">Beranda</a> */}
                        </div>
                        <div className="flex-none">
                            <div className="dropdown dropdown-end">
                                <div tabIndex={0} role="button" className="btn btn-ghost avatar">
                                    {userLogin?.nama_pegawai} <FontAwesomeIcon icon={faChevronDown} /> <br/>
                                </div>
                                <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-white rounded-box w-52">
                                    <li><a onClick={handleLogout}>Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    {children}
                </div>
                <Sidebar>
                    {children}
                </Sidebar>
            </div>
        </>
    )
}