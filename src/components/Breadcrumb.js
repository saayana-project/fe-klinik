import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Breadcrumb({ breadcrumbData }) {
    return (
        <>
            <div className="text-sm breadcrumbs mb-3">
                <ul>
                    {
                        breadcrumbData.map((row, i) => (
                            <li key={i}>
                                <a href={row.link} className="font-medium">
                                    {row.value}
                                </a>
                            </li>
                        ))
                    }
                </ul>
            </div>
        </>
    )
}