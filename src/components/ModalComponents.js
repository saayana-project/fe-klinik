import { useEffect, useState } from "react"
import Modal from "./Modal"
import apiService from "../../services/api.service"
import moment from "moment"
import { increaseNumber } from "@/utils/increaseNumber"
import Swal from "sweetalert2"
import { useRouter } from "next/router"

export const ModalPasienDoktorBaru = ({ isOpen, isClose, onGetDataPasien, onLoading }) => {
    const [noRegis, setNoRegis] = useState(null)
    const [noAntrian, setNoAntrian] = useState(null)
    const [nik, setNik] = useState(null)
    const [namaPasien, setNamaPasien] = useState('')
    const [tglLahir, setTglLahir] = useState('')
    const [generateUmur, setGenerateUmur] = useState('');
    const [namaPJ, setNamaPJ] = useState('');
    const [dataStatusPJ, setDataStatusPJ] = useState([]);
    const [statusPJ, setStatusPJ] = useState('');
    const [noHP, setNoHp] = useState('');
    const [pekerjaan, setPekerjaan] = useState('');
    const [alamat, setAlamat] = useState('');
    const [dataGolDarah, setDataGolDarah] = useState([]);
    const [golDarah, setGolDarah] = useState('');
    const [tinggiBadan, setTinggiBadan] = useState('');
    const [beratBadan, setBeratBadan] = useState('');

    useEffect(() => {
        const data = async () => {
            await getDataNoAntrian();
            await getDataNoReg();
        }

        data()
        getStatusPJ();
        getGolDarah();
    }, [])

    useEffect(() => {
        if (tglLahir) {
            const umur = hitungUmur(tglLahir);
            if (umur.years < 1) {
                setGenerateUmur(`${umur.months} bulan`);
            } else {
                setGenerateUmur(`${umur.years} tahun`);
            }
        }
    }, [tglLahir]);

    const getDataNoAntrian = () => {
        apiService.getNoAntrian(`?status=1`)
            .then((response) => {
                const data = response.data.data;
                const date = moment(new Date).format('YYMMDD');
                const resultData = data.no_antrian == null ? date + '001' : date + increaseNumber(data?.no_antrian?.substring(7, 10))

                setNoAntrian(resultData)
            })
    }

    const getDataNoReg = () => {
        apiService.getNoReg()
            .then((response) => {
                const data = response.data.data;
                const date = moment(new Date).format('YYMMDD');
                const resultData = data.no_reg == null ? '00001' : increaseNumber(data?.no_reg?.substring(6, 11))

                setNoRegis(date + resultData)
            })
    }

    const getStatusPJ = () => {
        const data = [
            { id: 1, status_pj: 'Orangtua' },
            { id: 2, status_pj: 'Kerabat' },
            { id: 3, status_pj: 'Wali' },
        ]

        setDataStatusPJ(data)
    }

    const getGolDarah = () => {
        const data = [
            { id: 1, nama: 'O+' },
            { id: 2, nama: 'A+' },
            { id: 3, nama: 'B+' },
            { id: 4, nama: 'AB+' },
            { id: 5, nama: 'O-' },
            { id: 6, nama: 'A-' },
            { id: 7, nama: 'B-' },
            { id: 8, nama: 'AB-' },
        ]

        setDataGolDarah(data)
    }

    function hitungUmur(tglLahir) {
        const lahir = new Date(tglLahir);
        const hariIni = new Date();
        const selisihBulan = hariIni.getMonth() - lahir.getMonth() + (12 * (hariIni.getFullYear() - lahir.getFullYear()));
        const tahun = Math.floor(selisihBulan / 12);
        const bulan = selisihBulan % 12;
        return { years: tahun, months: bulan };
    }

    const submitPasien = (e) => {
        e.preventDefault();

        isClose();
        onLoading(true)

        const today = new Date()

        const data = {
            'no_antrian': 'D' + noAntrian,
            'no_reg': noRegis,
            'nik': nik,
            'nama': namaPasien,
            'tgl_lahir': tglLahir,
            'umur_pasien': generateUmur,
            'nama_pj': namaPJ,
            'status_pj': statusPJ,
            'no_hp': noHP,
            'pekerjaan': pekerjaan,
            'alamat': alamat,
            'golongan_darah': golDarah,
            'tinggi_badan': tinggiBadan,
            'berat_badan': beratBadan,
            'status': 1,
            'tanggal': moment(today).format('YYYY-MM-DD')
        }

        apiService.postPasienDoktorBaru(data)
            .then((response) => {
                onLoading(false);
                onGetDataPasien();
                getDataNoAntrian();
                getDataNoReg();
                setNoAntrian('')
                setNoRegis('')
                setNik(null)
                setNamaPasien('')
                setTglLahir('')
                setGenerateUmur('')
                setNamaPJ('')
                setStatusPJ('')
                setNoHp('')
                setPekerjaan('')
                setAlamat('')
                setGolDarah('')
                setTinggiBadan('')
                setBeratBadan('')
                getStatusPJ()
                getGolDarah()
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                onLoading(false);
                onGetDataPasien();
                getDataNoAntrian();
                getDataNoReg();
                setNoAntrian('')
                setNoRegis('')
                setNik(null)
                setNamaPasien('')
                setTglLahir('')
                setGenerateUmur('')
                setNamaPJ('')
                setStatusPJ('')
                setNoHp('')
                setPekerjaan('')
                setAlamat('')
                setGolDarah('')
                setTinggiBadan('')
                setBeratBadan('')
                getStatusPJ()
                getGolDarah()
            })
    }

    return (
        <Modal
            title={'Pendaftaran Pasien Baru'}
            subtitle={'Pendaftaran pasien baru untuk antrian doktor'}
            openModal={isOpen}
        >
            <form onSubmit={submitPasien}>
                <div className="grid grid-rows-1">
                    <div className="grid grid-cols-1 gap-2">
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">No. Antrian :</span>
                            </div>
                            <input className="input input-bordered w-full font-bold" type="text" name="no_antrian" value={noAntrian?.slice(6)} disabled autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">No. Registrasi :</span>
                            </div>
                            <input className="input input-bordered w-full font-bold" type="text" value={noRegis} name="no_reg" disabled autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Nama Pasien :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan nama pasien" value={namaPasien} name="nama_pasien" onChange={(e) => setNamaPasien(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">NIK :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan NIK pasien" value={nik} name="nik" onChange={(e) => setNik(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Tanggal Lahir :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="date" value={tglLahir} name="tgl_lahir" onChange={(e) => setTglLahir(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Umur Pasien :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" value={generateUmur} name="tgl_lahir" autoComplete="off" readOnly />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Nama Penanggung Jawab :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan nama penanggung jawab pasien" value={namaPJ} onChange={(e) => setNamaPJ(e.target.value)} name="nama_pj" autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Status Penanggung Jawab :</span>
                            </div>
                            <select className="select select-bordered w-full font-medium" name="status_pj" value={statusPJ} onChange={(e) => setStatusPJ(e.target.value)} >
                                <option selected value={''}>Pilih status penanggung jawab</option>
                                {dataStatusPJ.map(items => (
                                    <option key={items.id} value={items.status_pj}>{items.status_pj}</option>
                                ))}
                            </select>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">No. HP :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan nomor hp" value={noHP} name="no_hp" onChange={(e) => setNoHp(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Pekerjaan :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan nama pekerjaan" value={pekerjaan} name="pekerjaan" onChange={(e) => setPekerjaan(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Alamat :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan alamat pasien" value={alamat} name="alamat" onChange={(e) => setAlamat(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Golongan Darah :</span>
                            </div>
                            <select className="select select-bordered w-full font-medium" name="gol_darah" value={golDarah} onChange={(e) => setGolDarah(e.target.value)} >
                                <option selected value={''}>Pilih golongan darah pasien</option>
                                {dataGolDarah.map(items => (
                                    <option key={items.id} value={items.nama}>{items.nama}</option>
                                ))}
                            </select>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Tinggi Badan :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan tinggi badan pasien" value={tinggiBadan} name="tinggi_badan" onChange={(e) => setTinggiBadan(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Berat Badan :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan berat badan pasien" value={beratBadan} name="berat_badan" onChange={(e) => setBeratBadan(e.target.value)} autoComplete="off" />
                        </div>
                    </div>

                    <div className="mt-10 text-end">
                        <button type="button" className="btn btn-error me-2" onClick={isClose}>Batal</button>
                        <button type="submit" className="btn btn-info">Simpan</button>
                    </div>
                </div>
            </form>
        </Modal>
    )
}

export const ModalPasienBidanBaru = ({ isOpen, isClose, onGetDataPasien, onLoading }) => {
    const [noRegis, setNoRegis] = useState(null)
    const [noAntrian, setNoAntrian] = useState(null)
    const [nik, setNik] = useState(null)
    const [namaPasien, setNamaPasien] = useState('')
    const [tglLahir, setTglLahir] = useState('')
    const [generateUmur, setGenerateUmur] = useState('');
    const [namaPJ, setNamaPJ] = useState('');
    const [dataStatusPJ, setDataStatusPJ] = useState([]);
    const [statusPJ, setStatusPJ] = useState('');
    const [noHP, setNoHp] = useState('');
    const [pekerjaan, setPekerjaan] = useState('');
    const [alamat, setAlamat] = useState('');
    const [dataGolDarah, setDataGolDarah] = useState([]);
    const [golDarah, setGolDarah] = useState('');
    const [tinggiBadan, setTinggiBadan] = useState('');
    const [beratBadan, setBeratBadan] = useState('');
    const [dataTindakan, setDataTindakan] = useState([]);
    const [tindakan, setTindakan] = useState('');

    useEffect(() => {
        const data = async () => {
            await getDataNoAntrian();
            await getDataNoReg();
        }

        data()
        getStatusPJ();
        getGolDarah();
        getDataTindakan();
    }, [])

    useEffect(() => {
        if (tglLahir) {
            const umur = hitungUmur(tglLahir);
            if (umur.years < 1) {
                setGenerateUmur(`${umur.months} bulan`);
            } else {
                setGenerateUmur(`${umur.years} tahun`);
            }
        }
    }, [tglLahir]);

    const getDataNoAntrian = () => {
        apiService.getNoAntrian(`?status=2`)
            .then((response) => {
                const data = response.data.data;
                const date = moment(new Date).format('YYMMDD');
                const resultData = data.no_antrian == null ? date + '001' : date + increaseNumber(data?.no_antrian?.substring(7, 10))

                setNoAntrian(resultData)
            })
    }

    const getDataNoReg = () => {
        apiService.getNoReg()
            .then((response) => {
                const data = response.data.data;
                const date = moment(new Date).format('YYMMDD');
                const resultData = data.no_reg == null ? '00001' : increaseNumber(data?.no_reg?.substring(6, 11))

                setNoRegis(date + resultData)
            })
    }

    const getStatusPJ = () => {
        const data = [
            { id: 1, status_pj: 'Orangtua' },
            { id: 2, status_pj: 'Kerabat' },
            { id: 3, status_pj: 'Wali' },
        ]

        setDataStatusPJ(data)
    }

    const getGolDarah = () => {
        const data = [
            { id: 1, nama: 'O+' },
            { id: 2, nama: 'A+' },
            { id: 3, nama: 'B+' },
            { id: 4, nama: 'AB+' },
            { id: 5, nama: 'O-' },
            { id: 6, nama: 'A-' },
            { id: 7, nama: 'B-' },
            { id: 8, nama: 'AB-' },
        ]

        setDataGolDarah(data)
    }

    const getDataTindakan = () => {
        const data = [
            { id: 1, nama: 'Umum' },
            { id: 2, nama: 'Imunisasi/KMS' },
            { id: 3, nama: 'Kehamilan' },
        ]

        setDataTindakan(data)
    }

    function hitungUmur(tglLahir) {
        const lahir = new Date(tglLahir);
        const hariIni = new Date();
        const selisihBulan = hariIni.getMonth() - lahir.getMonth() + (12 * (hariIni.getFullYear() - lahir.getFullYear()));
        const tahun = Math.floor(selisihBulan / 12);
        const bulan = selisihBulan % 12;
        return { years: tahun, months: bulan };
    }

    const submitPasien = (e) => {
        e.preventDefault();

        isClose();
        onLoading(true)

        const today = new Date()

        const data = {
            'no_antrian': 'B' + noAntrian,
            'no_reg': noRegis,
            'nik': nik,
            'nama': namaPasien,
            'tgl_lahir': tglLahir,
            'umur_pasien': generateUmur,
            'nama_pj': namaPJ,
            'status_pj': statusPJ,
            'no_hp': noHP,
            'pekerjaan': pekerjaan,
            'alamat': alamat,
            'golongan_darah': golDarah,
            'tinggi_badan': tinggiBadan,
            'berat_badan': beratBadan,
            'status': 2,
            'tanggal': moment(today).format('YYYY-MM-DD'),
            'tindakan': tindakan
        }

        apiService.postPasienBidanBaru(data)
            .then((response) => {
                onLoading(false);
                onGetDataPasien();
                getDataNoAntrian();
                getDataNoReg();
                setNoAntrian('')
                setNoRegis('')
                setNik(null)
                setNamaPasien('')
                setTglLahir('')
                setGenerateUmur('')
                setNamaPJ('')
                setStatusPJ('')
                setNoHp('')
                setPekerjaan('')
                setAlamat('')
                setGolDarah('')
                setTinggiBadan('')
                setBeratBadan('')
                setTindakan('')
                getStatusPJ()
                getGolDarah()
                getDataTindakan()
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                onLoading(false);
                onGetDataPasien();
                getDataNoAntrian();
                getDataNoReg();
                setNoAntrian('')
                setNoRegis('')
                setNik(null)
                setNamaPasien('')
                setTglLahir('')
                setGenerateUmur('')
                setNamaPJ('')
                setStatusPJ('')
                setNoHp('')
                setPekerjaan('')
                setAlamat('')
                setGolDarah('')
                setTinggiBadan('')
                setBeratBadan('')
                setTindakan('')
                getStatusPJ()
                getGolDarah()
                getDataTindakan()
            })
    }

    return (
        <Modal
            title={'Pendaftaran Pasien Baru'}
            subtitle={'Pendaftaran pasien baru untuk antrian doktor'}
            openModal={isOpen}
        >
            <form onSubmit={submitPasien}>
                <div className="grid grid-rows-1">
                    <div className="grid grid-cols-1 gap-2">
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">No. Antrian :</span>
                            </div>
                            <input className="input input-bordered w-full font-bold" type="text" name="no_antrian" value={noAntrian?.slice(6)} disabled autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">No. Registrasi :</span>
                            </div>
                            <input className="input input-bordered w-full font-bold" type="text" value={noRegis} name="no_reg" disabled autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Nama Pasien :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan nama pasien" value={namaPasien} name="nama_pasien" onChange={(e) => setNamaPasien(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">NIK :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan NIK pasien" value={nik} name="nik" onChange={(e) => setNik(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Tanggal Lahir :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="date" value={tglLahir} name="tgl_lahir" onChange={(e) => setTglLahir(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Umur Pasien :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" value={generateUmur} name="tgl_lahir" autoComplete="off" readOnly />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Nama Penanggung Jawab :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan nama penanggung jawab pasien" value={namaPJ} onChange={(e) => setNamaPJ(e.target.value)} name="nama_pj" autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Status Penanggung Jawab :</span>
                            </div>
                            <select className="select select-bordered w-full font-medium" name="status_pj" value={statusPJ} onChange={(e) => setStatusPJ(e.target.value)} >
                                <option selected value={''}>Pilih status penanggung jawab</option>
                                {dataStatusPJ.map(items => (
                                    <option key={items.id} value={items.status_pj}>{items.status_pj}</option>
                                ))}
                            </select>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">No. HP :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan nomor hp" value={noHP} name="no_hp" onChange={(e) => setNoHp(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Pekerjaan :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan nama pekerjaan" value={pekerjaan} name="pekerjaan" onChange={(e) => setPekerjaan(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Alamat :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="text" placeholder="Masukkan alamat pasien" value={alamat} name="alamat" onChange={(e) => setAlamat(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Golongan Darah :</span>
                            </div>
                            <select className="select select-bordered w-full font-medium" name="gol_darah" value={golDarah} onChange={(e) => setGolDarah(e.target.value)} >
                                <option selected value={''}>Pilih golongan darah pasien</option>
                                {dataGolDarah.map(items => (
                                    <option key={items.id} value={items.nama}>{items.nama}</option>
                                ))}
                            </select>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Tinggi Badan :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan tinggi badan pasien" value={tinggiBadan} name="tinggi_badan" onChange={(e) => setTinggiBadan(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Berat Badan :</span>
                            </div>
                            <input className="input input-bordered w-full font-medium" type="number" placeholder="Masukkan berat badan pasien" value={beratBadan} name="berat_badan" onChange={(e) => setBeratBadan(e.target.value)} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Pemilihan Tindakan :</span>
                            </div>
                            <select className="select select-bordered w-full font-medium" name="gol_darah" value={tindakan} onChange={(e) => setTindakan(e.target.value)} >
                                <option selected value={''}>Pilih tindakan pasien</option>
                                {dataTindakan.map(items => (
                                    <option key={items.id} value={items.id}>{items.nama}</option>
                                ))}
                            </select>
                        </div>
                    </div>

                    <div className="mt-10 text-end">
                        <button type="button" className="btn btn-error me-2" onClick={isClose}>Batal</button>
                        <button type="submit" className="btn btn-info">Simpan</button>
                    </div>
                </div>
            </form>
        </Modal>
    )
}

export const ModalRegistrasiKehamilan = ({ isOpen, isClose, onGetDataPasien, isData, onLoading }) => {
    const router = useRouter();
    const [namaSuami, setNamaSuami] = useState('')
    const [hpht, setHpht] = useState('')
    const [hpt, setHpt] = useState('')
    const [lingkarLenganAtas, setLingkarLenganAtas] = useState('')
    const [selectedValue, setSelectedValue] = useState('');
    const [riwayatPenyakitIbu, setRiwayatPenyakitIbu] = useState('');
    const [riwayatAlergi, setRiwayatAlergi] = useState('');
    const [hamilKe, setHamilKe] = useState('');
    const [jmlKehamilan, setJmlKehamilan] = useState('');
    const [jmlKeguguran, setJmlKeguguran] = useState('');
    const [jmlAnakHidup, setJmlAnakHidup] = useState('');
    const [jmlLahirMati, setJmlLahirMati] = useState('');
    const [jmlKurangBulan, setJmlKurangBulan] = useState('');
    const [jmlKehamilanTerakhir, setJmlKehamilanTerakhir] = useState('');
    const [statImunisasiTT, setStatImunisasiTT] = useState('');
    const [imunisasiTT, setImunisasiTT] = useState('');
    const [penolongPersalinan, setPenolongPersalinan] = useState('');
    const [caraPersalinan, setCaraPersalinan] = useState('');
    const [tindakanPersalinan, setTindakanPersalinan] = useState('');

    const submitKehamilan = (e) => {
        e.preventDefault();
        onLoading(true)
        isClose();

        const data = {
            'no_reg': isData?.no_reg,
            'nama_suami': namaSuami,
            'hpht': hpht,
            'tgl_htp': hpt,
            'lingkar_lengan_atas': lingkarLenganAtas,
            'berat_badan': isData?.berat_badan,
            'tinggi_badan': isData?.tinggi_badan,
            'kontrasepsi_sebelum_hamil': selectedValue,
            'riwayat_penyakit': riwayatPenyakitIbu,
            'riwayat_alergi': riwayatAlergi,
            'hamil_ke': hamilKe,
            'jml_kehamilan': jmlKehamilan,
            'jml_keguguran': jmlKeguguran,
            'jml_anak_hdp': jmlAnakHidup,
            'jml_lahir_mati': jmlLahirMati,
            'jml_anak_lahir_kurang_bulan': jmlKurangBulan,
            'jarak_kehamilan': jmlKehamilanTerakhir,
            'stat_imunisasi_tt': statImunisasiTT,
            'imunisasi_tt_terakhir': imunisasiTT,
            'penolong_persalinan_terakhir': penolongPersalinan,
            'cara_persalinan_terakhir': caraPersalinan,
            'tindakan_persalinan_terakhir': tindakanPersalinan,
        }

        // console.log(data)

        apiService.postKehamilan(data)
            .then((response) => {
                onLoading(false);
                localStorage.setItem('pasien_kehamilan', JSON.stringify(isData?.no_reg))
                router.push('/klinik/bidan/daftar-pasien/rekam-kehamilan')
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                onLoading(false);
                setNamaSuami('')
                setHpht('')
                setHpt('')
                setLingkarLenganAtas('')
                setSelectedValue('')
                setRiwayatPenyakitIbu('')
                setRiwayatAlergi('')
                setHamilKe('')
                setJmlKehamilan('')
                setJmlKeguguran('')
                setJmlAnakHidup('')
                setJmlLahirMati('')
                setJmlKurangBulan('')
                setJmlKehamilanTerakhir('')
                setStatImunisasiTT('')
                setImunisasiTT('')
                setPenolongPersalinan('')
                setCaraPersalinan('')
                setTindakanPersalinan('')
            })
    }

    return (
        <Modal
            title={'Registrasi Kehamilan'}
            subtitle={null}
            openModal={isOpen}
        >
            <form onSubmit={submitKehamilan}>
                <div className="grid grid-rows-1">
                    <div className="grid grid-cols-1 gap-2">
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Nama Suami :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="nama_suami" placeholder="Masukkan nama suami" onChange={(e) => setNamaSuami(e.target.value)} value={namaSuami} autoComplete="off" required />
                        </div>
                        <div className="w-full">
                            <div className="label">
                                <span className="label-text font-semibold">Hari Pertama Haid Terakhir (HPHT) :</span>
                            </div>
                            <input className="input input-bordered w-full" type="date" name="hpht" placeholder="Haid Pertama Haid Terakhir" onChange={(e) => setHpht(e.target.value)} value={hpht} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Hari Taksiran Persalinan (HTP) :</span>
                            </div>
                            <input className="input input-bordered w-full" type="date" name="hpht" placeholder="Masukkan hari taksiran persalinan (HTP)" onChange={(e) => setHpt(e.target.value)} value={hpt} autoComplete="off" />
                        </div>
                        <div className="flex gap-3">
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Lingkar Lengan Atas :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="lingkar_lengan_atas" placeholder="Lingkar lengan atas (cm)" onChange={(e) => setLingkarLenganAtas(e.target.value)} value={lingkarLenganAtas} autoComplete="off" />
                            </div>
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Tinggi Badan :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="ht" placeholder="Haid terakhir" value={isData?.tinggi_badan} autoComplete="off" readOnly />
                            </div>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Penggunaan Kontrasepsi Sebelum Kehamilan :</span>
                            </div>
                            <div className="flex gap-5">
                                <div className="form-control">
                                    <label className="label cursor-pointer">
                                        <input
                                            type="radio"
                                            name="radio-10"
                                            className="radio radio-primary"
                                            value="Belum Pernah"
                                            onChange={(e) => setSelectedValue(e.target.value)}
                                            checked={selectedValue === 'Belum Pernah'}
                                        />
                                        <span className="label-text font-medium ms-2">Belum Pernah</span>
                                    </label>
                                </div>
                                <div className="form-control">
                                    <label className="label cursor-pointer">
                                        <input
                                            type="radio"
                                            name="radio-10"
                                            className="radio radio-primary"
                                            value="Pernah"
                                            onChange={(e) => setSelectedValue(e.target.value)}
                                            checked={selectedValue === 'Pernah'}
                                        />
                                        <span className="label-text font-medium ms-2">Pernah</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Riwayat Penyakit Yang Diderita Ibu :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="riwayat_penyakit_ibu" placeholder="Masukkan riwayat penyakit yang diderita ibu" onChange={(e) => setRiwayatPenyakitIbu(e.target.value)} value={riwayatPenyakitIbu} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Riwayat Alergi :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="riwayat_alergi" placeholder="Masukkan riwayat alergi" onChange={(e) => setRiwayatAlergi(e.target.value)} value={riwayatAlergi} autoComplete="off" />
                        </div>
                        <div className="flex gap-5">
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Hamil Ke :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="hamil_ke" onChange={(e) => setHamilKe(e.target.value)} value={hamilKe} autoComplete="off" />
                            </div>
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Jumlah Kehamilan :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="jml_kehamilan" onChange={(e) => setJmlKehamilan(e.target.value)} value={jmlKehamilan} autoComplete="off" />
                            </div>
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Jumlah Keguguran :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="jml_keguguran" onChange={(e) => setJmlKeguguran(e.target.value)} value={jmlKeguguran} autoComplete="off" />
                            </div>
                        </div>
                        <div className="flex gap-5">
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Jumlah Anak Hidup :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="jml_lahir_hidup" onChange={(e) => setJmlAnakHidup(e.target.value)} value={jmlAnakHidup} autoComplete="off" />
                            </div>
                            <div className="w-full">
                                <div className="label">
                                    <span className="label-text font-semibold">Jumlah Lahir Mati :</span>
                                </div>
                                <input className="input input-bordered w-full" type="text" name="jml_lahir_mati" onChange={(e) => setJmlLahirMati(e.target.value)} value={jmlLahirMati} autoComplete="off" />
                            </div>
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Jumlah Anak Lahir Kurang Bulan :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="jml_kurang_bulan" onChange={(e) => setJmlKurangBulan(e.target.value)} value={jmlKurangBulan} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Jarak Kehamilan Ini Dengan Kehamilan Terakhir :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="jml_kehamilan_terakhir" onChange={(e) => setJmlKehamilanTerakhir(e.target.value)} value={jmlKehamilanTerakhir} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Status Imunisasi TT :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="stat_imunisasi_tt" onChange={(e) => setStatImunisasiTT(e.target.value)} value={statImunisasiTT} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Imunisasi TT Terakhir :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="imunisasi_tt_terakhir" onChange={(e) => setImunisasiTT(e.target.value)} value={imunisasiTT} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Penolong Persalinan Terakhir :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="penolong_persalinan_terakhir" onChange={(e) => setPenolongPersalinan(e.target.value)} value={penolongPersalinan} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Cara Persalinan Terakhir :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="cara_persalinan_terakhir" onChange={(e) => setCaraPersalinan(e.target.value)} value={caraPersalinan} autoComplete="off" />
                        </div>
                        <div>
                            <div className="label">
                                <span className="label-text font-semibold">Tindakan Persalinan Terakhir :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="cara_persalinan_terakhir" onChange={(e) => setTindakanPersalinan(e.target.value)} value={tindakanPersalinan} autoComplete="off" />
                        </div>
                    </div>

                    <div className="mt-10 text-end">
                        <button type="button" className="btn btn-error me-2" onClick={isClose}>Batal</button>
                        <button type="submit" className="btn btn-info">Simpan</button>
                    </div>
                </div>
            </form>
        </Modal>
    )
}

export const ModalTambahKMS = ({ isOpen, isClose, onGetDataPasien, onGetDataKMS, isData, onLoading }) => {
    const [hpht, setHpht] = useState('')
    const [dataBulan, setDataBulan] = useState([])
    const [bulanPenimbangan, setBulanPenimbangan] = useState('')
    const [generateUmur, setGenerateUmur] = useState('');
    const [umur, setUmur] = useState('')
    const [asi, setAsi] = useState('')
    const [beratBadan, setBeratBadan] = useState('')

    useEffect(() => {
        getDataBulan();
        setUmur(parseInt(isData?.umur_pasien, 10));
        setBeratBadan(parseFloat(isData?.berat_badan))
    }, [isData]);

    useEffect(() => {
        if (isData?.tgl_lahir) {
            const umur = hitungUmur(isData?.tgl_lahir);
            if (umur.years < 1) {
                setGenerateUmur(`${umur.months} bulan`);
            } else {
                setGenerateUmur(`${umur.years} tahun`);
            }
        }
    }, [isData?.tgl_lahir]);

    function hitungUmur(tglLahir) {
        const lahir = new Date(tglLahir);
        const hariIni = new Date();
        const selisihBulan = hariIni.getMonth() - lahir.getMonth() + (12 * (hariIni.getFullYear() - lahir.getFullYear()));
        const tahun = Math.floor(selisihBulan / 12);
        const bulan = selisihBulan % 12;
        return { years: tahun, months: bulan };
    }

    const getDataBulan = () => {
        const data = [
            { id: '01', name: 'Januari' },
            { id: '02', name: 'Februari' },
            { id: '03', name: 'Maret' },
            { id: '04', name: 'April' },
            { id: '05', name: 'Mei' },
            { id: '06', name: 'Juni' },
            { id: '07', name: 'Juli' },
            { id: '08', name: 'Agustus' },
            { id: '09', name: 'September' },
            { id: '10', name: 'Oktober' },
            { id: '11', name: 'November' },
            { id: '12', name: 'Desember' },
        ]

        setDataBulan(data)
    }

    const submitKMS = (e) => {
        e.preventDefault();

        isClose();
        onLoading(true)

        const todayDay = moment(new Date()).format('DD');
        const todayYear = moment(new Date()).format('YYYY');

        const data = {
            'no_reg': isData?.no_reg,
            'bulan_penimbangan': todayYear + '-' + bulanPenimbangan + '-' + todayDay,
            'umur_pasien': generateUmur,
            'berat_badan': beratBadan?.toString(),
            'nt': beratBadan >= isData?.berat_badan ? 'N' : 'T',
            'asi_ekslusif': asi,
        }

        apiService.postKMS(data)
            .then((response) => {
                onLoading(false);
                setBulanPenimbangan('')
                setUmur('')
                setAsi('')
                setBeratBadan('')
                onGetDataPasien()
                onGetDataKMS()
            })
            .catch((err) => {
                if (err.response && err.response.data && typeof err.response.data === 'object') {
                    const errors = err.response.data;
                    let errorMessage = Object.keys(errors).map(key => {
                        if (Array.isArray(errors[key])) {
                            return `${key}: ${errors[key].join(', ')}`;
                        } else {
                            return `${key}: ${errors[key].toString()}`;
                        }
                    }).join('<br/>');

                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${errorMessage}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else if (err.message) {
                    Swal.fire({
                        title: 'Gagal!',
                        html: `<i>${err.message}</i>`,
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                } else {
                    Swal.fire({
                        title: 'Gagal!',
                        html: '<i>An unexpected error occurred</i>',
                        icon: 'error',
                        confirmButtonColor: '#0058a8',
                    });
                }
                onLoading(false);
                setBulanPenimbangan('')
                setUmur('')
                setAsi('')
                setBeratBadan('')
                onGetDataPasien()
                onGetDataKMS()
            })
    }

    return (
        <Modal
            title={'Tambah KMS'}
            subtitle={null}
            openModal={isOpen}
        >
            <form onSubmit={submitKMS}>
                <div className="grid grid-rows-1">
                    <div className="grid grid-cols-1 gap-2">
                        <div className="w-full">
                            <div className="label">
                                <span className="label-text font-semibold">Bulan Penimbangan :</span>
                            </div>
                            <select className="select select-bordered w-full font-medium" name="bulan_penimbangan" value={bulanPenimbangan} onChange={(e) => setBulanPenimbangan(e.target.value)} required>
                                <option selected value={''}>Pilih bulan</option>
                                {dataBulan.map(items => (
                                    <option key={items.id} value={items.id}>{items.name}</option>
                                ))}
                            </select>
                        </div>
                        <div className="w-full">
                            <div className="label">
                                <span className="label-text font-semibold">Umur (bulan) :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="umur" placeholder="Masukkan umur pasien" onChange={(e) => setGenerateUmur(e.target.value)} value={generateUmur} autoComplete="off" />
                        </div>
                        <div className="w-full">
                            <div className="label">
                                <span className="label-text font-semibold">Berat Badan (kg) :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="umur" placeholder="Masukkan umur pasien" onChange={(e) => setBeratBadan(e.target.value)} value={beratBadan} autoComplete="off" />
                        </div>
                        <div className="w-full">
                            <div className="label">
                                <span className="label-text font-semibold">Asi Ekslusif :</span>
                            </div>
                            <input className="input input-bordered w-full" type="text" name="asi_ekslusif" placeholder="Masukkan asi ekslusif" onChange={(e) => setAsi(e.target.value)} value={asi} autoComplete="off" />
                        </div>
                    </div>

                    <div className="mt-10 text-end">
                        <button type="button" className="btn btn-error me-2" onClick={isClose}>Batal</button>
                        <button type="submit" className="btn btn-info">Simpan</button>
                    </div>
                </div>
            </form>
        </Modal>
    )
}