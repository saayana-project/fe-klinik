// import Swal from 'sweetalert2';
import api from './private-api.service';

const API_URL = process.env.NEXT_PUBLIC_URL_API;

class ApiService {
    //AUTH
    postLogin(params) {
        return api.post(API_URL + '/user/login', params)
    }

    // REF
    getJabatan(params) {
        return api.get(API_URL + '/super-admin/jabatan' + params)
    }

    getDiagnosa() {
        return api.get(API_URL + '/ref/diagnosa')
    }

    getTherapy() {
        return api.get(API_URL + '/ref/therapy')
    }

    getObat() {
        return api.get(API_URL + '/ref/obat')
    }

    // SUPER ADMIN
    getPegawai(params) {
        return api.get(API_URL + '/super-admin/pegawai' + params)
    }

    postPegawai(body) {
        return api.post(API_URL + '/super-admin/add-pegawai', body)
    }

    deletePegawai(body) {
        return api.post(API_URL + '/super-admin/delete-pegawai', body)
    }

    // ADMINISTRASI
    getNoAntrian(params) {
        return api.get(API_URL + '/administrasi/no_antrian' + params)
    }

    getNoReg() {
        return api.get(API_URL + '/administrasi/no_reg')
    }

    getJmlPasien() {
        return api.get(API_URL + '/administrasi/count-antrian')
    }

    postCekNIK(body) {
        return api.post(API_URL + '/administrasi/cek-nik', body)
    }

    getPasien(params) {
        return api.get(API_URL + '/administrasi/antrian' + params)
    }

    postPasienDoktorBaru(body) {
        return api.post(API_URL + '/administrasi/doktor/add-pasien/baru', body)
    }

    postPasienBidanBaru(body) {
        return api.post(API_URL + '/administrasi/bidan/add-pasien/baru', body)
    }

    postPasienExist(body) {
        return api.post(API_URL + '/administrasi/exist-pasien', body)
    }

    // DOKTOR
    getJmlPasienDoktor() {
        return api.get(API_URL + '/doktor/count-antrian')
    }

    getPasienDoktor(params) {
        return api.get(API_URL + '/doktor/pasien' + params)
    }

    // BIDAN
    getJmlPasienBidan() {
        return api.get(API_URL + '/bidan/count-antrian')
    }

    getPasienBidan(params) {
        return api.get(API_URL + '/bidan/pasien' + params)
    }

    getDetailPasienBidan(params) {
        return api.get(API_URL + '/bidan/pasien/penanganan' + params)
    }

    getKMS(params) {
        return api.get(API_URL + '/bidan/pasien/kms' + params)
    }

    getImunisasi(params) {
        return api.get(API_URL + '/bidan/pasien/imunisasi' + params)
    }

    postKMS(body) {
        return api.post(API_URL + '/bidan/pasien/add-kms', body)
    }

    putPenangananBidan(id) {
        return api.put(API_URL + `/bidan/pasien/penanganan-selesai/${id}`)
    }

    postUpdateImunisasi(body) {
        return api.put(API_URL + `/bidan/pasien/update-imunisasi`, body)
    }

    postKehamilan(body) {
        return api.post(API_URL + `/bidan/pasien/add-kehamilan`, body)
    }

    getDetailKehamilan(params) {
        return api.get(API_URL + '/bidan/pasien/kehamilan' + params)
    }
}

export default new ApiService();