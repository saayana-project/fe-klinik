import axios from "axios";
import Swal from "sweetalert2";

const instance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_URL_API,
    headers: {
        "Accept": "application/json",
    },
});

// instance.interceptors.response.use(
//     (res) => {
//         return Swal.fire({
//             icon: "success",
//             title: "Berhasil!",
//             text: res.data?.message,
//             confirmButtonColor: "#a6cbff"
//         });;
//     },
//     async (err) => {
//         // return console.log();
//         return Swal.fire({
//             icon: "error",
//             title: "Terjadi Kesalahan!",
//             text: err.response.data?.message,
//             confirmButtonColor: "#a6cbff"
//         });
//         // const originalConfig = err.config;
//         // if (err.response.status === 401 && !originalConfig._retry) {
//         //     Swal.fire({
//         //         icon: 'error',
//         //         title: 'Mohon Maaf',
//         //         text: 'Anda Tidak Memiliki Akses Untuk Mengakses Aplikasi!'
//         //     }).then(() => {
//         //         TokenService.removeUser();
//         //         window.location.href = '/'
//         //     })
//         // } else if (err.response.status === 403 && !originalConfig._retry) {
//         //     Swal.fire({
//         //         icon: 'error',
//         //         title: 'Mohon Maaf',
//         //         text: 'Mohon Login Kembali, Untuk Mengakses Aplikasi!'
//         //     }).then((res) => {
//         //         TokenService.removeUser();
//         //     })
//         // } else {
//         //     Swal.fire({
//         //         icon: 'error',
//         //         title: 'Mohon Maaf',
//         //         text: 'Terjadi Kesalahan Pada Server!'
//         //     }).then((res) => {
//         //         TokenService.removeUser();
//         //     })
//         // }
//         // return Promise.reject(err);
//     }
// );
export default instance